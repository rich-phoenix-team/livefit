#!/bin/bash

# set -e

echo "\nGetting and compiling dependencies"
mix deps.get

# echo "\nInstalling JS..."
# cd assets && yarn install --check-files
# cd ..

# Wait for Postgres to become available.
# until psql -h livefit-db -U "postgres" -c '\q' 2>/dev/null; do
#   >&2 echo "Postgres is unavailable - sleeping"
#   sleep 1
# done

# Set up the database
mix ecto.create
mix ecto.migrate

mix phx.server