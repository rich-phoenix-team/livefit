defmodule LiveFit.FitnessFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `LiveFit.Fitness` context.
  """

  def unique_fitness_name, do: "my name#{System.unique_integer()}"
  def regular_fitness_name, do: "my name"

  def muscle_ids(muscles) when is_list(muscles), do: Enum.map(muscles, &(&1.id))
  def muscle_ids(muscle), do: [muscle.id]

  def category_fixture(attrs \\ %{}) do
    {:ok, category} =
      attrs
      |> Enum.into(%{
        title: unique_fitness_name()
      })
      |> LiveFit.Fitness.create_category()

    category
  end

  def muscle_fixture(attrs \\ %{}) do
    {:ok, muscle} =
      attrs
      |> Enum.into(%{
        name: unique_fitness_name()
      })
      |> LiveFit.Fitness.create_muscle()

    muscle
  end

  def workout_fixture(user), do: workout_fixture(%{}, user, [], %{id: ""})
  def workout_fixture(attrs \\ %{}, user, muscles, category) do
    {:ok, workout} =
      attrs
      |> Enum.into(%{
        description: "some description",
        link: "some link",
        title: unique_fitness_name(),
        equipment_needed: false,
        muscles: muscle_ids(muscles),
        category_id: category.id
      })
      |> LiveFit.Fitness.create_workout(user)

    workout
  end

end
