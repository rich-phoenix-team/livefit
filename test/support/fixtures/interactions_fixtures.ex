defmodule LiveFit.InteractionsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `LiveFit.Fitness` context.
  """

  def unique_interactions_name, do: "my name#{System.unique_integer()}"
  def regular_interactions_name, do: "my name"

  def comment_fixture(attrs \\ %{}, user, workout_id) do
    {:ok, comment} =
      attrs
      |> Enum.into(%{
        body: unique_interactions_name(),
        workout_id: workout_id
      })
      |> LiveFit.Interactions.create_comment(user)

    comment
  end


end
