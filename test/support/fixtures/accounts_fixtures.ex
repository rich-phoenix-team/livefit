defmodule LiveFit.AccountsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `LiveFit.Accounts` context.
  """

  def unique_user_email, do: "user#{System.unique_integer()}@example.com"
  def valid_user_password, do: "hello world!"
  def valid_trainee_role, do: :trainee
  def valid_trainer_role, do: :trainer
  def valid_admin_role, do: :admin

  def user_fixture(attrs \\ %{}) do
    {:ok, user} =
      attrs
      |> Enum.into(%{
        email: unique_user_email(),
        password: valid_user_password(),
        role: valid_trainee_role()
      })
      |> LiveFit.Accounts.register_user()

    user
  end

  def trainee_fixture(attrs \\ %{}) do
    {:ok, user} =
      attrs
      |> Enum.into(%{
        email: unique_user_email(),
        password: valid_user_password()
      })
      |> LiveFit.Accounts.register_trainee()

    user
  end

  def trainer_fixture(attrs \\ %{}) do
    {:ok, user} =
      attrs
      |> Enum.into(%{
        email: unique_user_email(),
        password: valid_user_password(),

      })
      |> LiveFit.Accounts.register_trainer()

    %{user | workout_count: 0}
  end

  def admin_fixture(attrs \\ %{}) do
    {:ok, user} =
      attrs
      |> Enum.into(%{
        email: unique_user_email(),
        password: valid_user_password()
      })
      |> LiveFit.Accounts.register_admin()

    user
  end

  def extract_user_token(fun) do
    {:ok, captured} = fun.(&"[TOKEN]#{&1}[TOKEN]")
    [_, token, _] = String.split(captured.body, "[TOKEN]")
    token
  end
end
