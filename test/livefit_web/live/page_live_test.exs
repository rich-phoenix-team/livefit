defmodule LiveFitWeb.PageLiveTest do
  use LiveFitWeb.ConnCase

  import Phoenix.LiveViewTest
  import LiveFit.AccountsFixtures

  test "disconnected and connected render", %{conn: conn} do
    conn = log_in_user(conn, admin_fixture())

    {:ok, page_live, disconnected_html} = live(conn, "/")
    assert disconnected_html =~ "Welcome to Phoenix!"
    assert render(page_live) =~ "Welcome to Phoenix!"
  end
end
