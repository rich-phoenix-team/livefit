defmodule LiveFitWeb.WorkoutLiveTest do
  use LiveFitWeb.ConnCase

  import Phoenix.LiveViewTest
  import LiveFit.AccountsFixtures
  import LiveFit.FitnessFixtures

  alias LiveFit.Interactions

  @create_attrs %{description: "some description", link: "some link", title: "some title", equipment_needed: "false", muscles: "", category_id: ""}
  @update_attrs %{description: "some updated description", link: "some updated link", title: "some updated title", equipment_needed: "true"}
  @invalid_attrs %{description: nil, link: nil, title: nil}
  @empty_search_attrs %{search_phrase: "", muscle_ids: "", category_id: "", equipment_needed: true}

  defp create_workout_and_trainer(%{conn: conn}) do
    user = trainer_fixture()
    muscles = [muscle_fixture(), muscle_fixture()]
    category = category_fixture()
    workout = workout_fixture(user, muscles, category)
    %{conn: log_in_user(conn, user), trainer: user, workout: workout, category: category, muscles: muscles}
  end

  defp create_workout_and_trainee(%{conn: conn}) do
    trainee = trainee_fixture()
    workout = workout_fixture(trainer_fixture())
    %{conn: log_in_user(conn, trainee), current_user: trainee, workout: workout}
  end

  defp put_category_and_muscles_to_attributes(attrs, category_id, muscle_ids) do
    attrs
    |> Map.put(:category_id, category_id)
    |> Map.put(:muscles, muscle_ids)
  end

  describe "Trainee" do
    setup [:create_workout_and_trainee]

    test "index - lists all workouts contains trainer email", %{conn: conn, workout: workout} do
      {:ok, index_live, html} = live(conn, Routes.workout_index_path(conn, :index))

      assert html =~ "Listing Workouts"
      assert index_live |> element("tr#workout-#{workout.id}") |> render() =~ workout.user.email
      assert html =~ workout.description
      assert html =~ workout.link
      assert html =~ workout.title
      assert html =~ to_string(workout.equipment_needed)
    end

    test "show - workout contains trainer email", %{conn: conn, workout: workout} do
      {:ok, show_live, html} = live(conn, Routes.workout_show_path(conn, :show, workout))

      assert html =~ "Show Workout"
      assert show_live |> element("main.container") |> render() =~ workout.user.email
      assert html =~ workout.description
      assert html =~ workout.link
      assert html =~ workout.title
      assert html =~ to_string(workout.equipment_needed)
    end

    test "index adds workout to favorite and removes it from favorites", %{conn: conn, workout: %{id: workout_id} = workout, current_user: current_user} do
      {:ok, index_live, html} = live(conn, Routes.workout_index_path(conn, :index))

      assert html =~ "Listing Workouts"

      assert get_index_button(index_live, workout.id, "favorite") |> has_element?()
      refute get_index_button(index_live, workout.id, "unfavorite") |> has_element?()

      get_index_button(index_live, workout.id, "favorite") |> render_click()
      assert [%LiveFit.Fitness.Workout{id: ^workout_id}] = Interactions.list_favorite_workouts(current_user)

      assert get_index_button(index_live, workout.id, "unfavorite") |> has_element?()
      refute get_index_button(index_live, workout.id, "favorite") |> has_element?()

      get_index_button(index_live, workout.id, "unfavorite") |> render_click()
      assert [] == Interactions.list_favorite_workouts(current_user)

      assert get_index_button(index_live, workout.id, "favorite") |> has_element?()
      refute get_index_button(index_live, workout.id, "unfavorite") |> has_element?()
    end

    test "show adds workout to favorite and removes it from favorites", %{conn: conn, workout: %{id: workout_id} = workout, current_user: current_user} do
      {:ok, show_live, html} = live(conn, Routes.workout_show_path(conn, :show, workout))

      assert html =~ "Show Workout"

      assert get_show_button(show_live, "favorite") |> has_element?()
      refute get_show_button(show_live, "unfavorite") |> has_element?()

      get_show_button(show_live, "favorite") |> render_click()
      assert [%LiveFit.Fitness.Workout{id: ^workout_id}] = Interactions.list_favorite_workouts(current_user)

      assert get_show_button(show_live, "unfavorite") |> has_element?()
      refute get_show_button(show_live, "favorite") |> has_element?()

      get_show_button(show_live, "unfavorite") |> render_click()
      assert [] == Interactions.list_favorite_workouts(current_user)

      assert get_show_button(show_live, "favorite") |> has_element?()
      refute get_show_button(show_live, "unfavorite") |> has_element?()
    end

    test "index likes and unlikes workout", %{conn: conn, workout: %{id: workout_id} = workout, current_user: current_user} do
      {:ok, index_live, html} = live(conn, Routes.workout_index_path(conn, :index))

      assert html =~ "Listing Workouts"

      assert get_index_button(index_live, workout.id, "like") |> has_element?()
      refute get_index_button(index_live, workout.id, "unlike") |> has_element?()

      get_index_button(index_live, workout.id, "like") |> render_click()
      assert [%LiveFit.Fitness.Workout{id: ^workout_id}] = Interactions.list_liked_workouts(current_user)

      assert get_index_button(index_live, workout.id, "unlike") |> has_element?()
      refute get_index_button(index_live, workout.id, "like") |> has_element?()

      get_index_button(index_live, workout.id, "unlike") |> render_click()
      assert [] == Interactions.list_liked_workouts(current_user)

      assert get_index_button(index_live, workout.id, "like") |> has_element?()
      refute get_index_button(index_live, workout.id, "unlike") |> has_element?()
    end

    test "show likes and unlikes workout", %{conn: conn, workout: %{id: workout_id} = workout, current_user: current_user} do
      {:ok, show_live, html} = live(conn, Routes.workout_show_path(conn, :show, workout))

      assert html =~ "Show Workout"

      assert get_show_button(show_live, "like") |> has_element?()
      refute get_show_button(show_live, "unlike") |> has_element?()

      get_show_button(show_live, "like") |> render_click()
      assert [%LiveFit.Fitness.Workout{id: ^workout_id}] = Interactions.list_liked_workouts(current_user)

      assert get_show_button(show_live, "unlike") |> has_element?()
      refute get_show_button(show_live, "like") |> has_element?()

      get_show_button(show_live, "unlike") |> render_click()
      assert [] == Interactions.list_liked_workouts(current_user)

      assert get_show_button(show_live, "like") |> has_element?()
      refute get_show_button(show_live, "unlike") |> has_element?()
    end

    test "workout views change increase by 1 when going to show page and stays at 1 even after reload", %{conn: conn, workout: %{id: workout_id} = workout} do
      {:ok, index_live, html} = live(conn, Routes.workout_index_path(conn, :index))
      assert html =~ "Listing Workouts"
      assert index_live |> element("tr#workout-#{workout_id} td:nth-child(10)") |> render() =~ "0"

      {:ok, show_live, html} = live(conn, Routes.workout_show_path(conn, :show, workout))
      assert html =~ "Show Workout"
      assert show_live |> element("main ul li:nth-child(8)") |> render() =~ "1"

      {:ok, show_live, html} = live(conn, Routes.workout_show_path(conn, :show, workout))
      assert html =~ "Show Workout"
      assert show_live |> element("main ul li:nth-child(8)") |> render() =~ "1"
    end

  end

  describe "Index" do
    setup [:create_workout_and_trainer]

    test "lists all workouts", %{conn: conn, workout: workout, category: category, muscles: muscles} do
      {:ok, index_live, html} = live(conn, Routes.workout_index_path(conn, :index))

      assert html =~ "Listing Workouts"
      refute index_live |> element("tr#workout-#{workout.id}") |> render() =~ workout.user.email
      assert html =~ workout.description
      assert html =~ workout.link
      assert html =~ workout.title
      assert html =~ to_string(workout.equipment_needed)
      assert html =~ category.title
      Enum.each(muscles, fn muscle ->
        assert html =~ muscle.name
      end)
    end

    test "searches workouts", %{conn: conn, workout: workout, trainer: trainer} do
      workout_with_equipment = workout_fixture(%{equipment_needed: true}, trainer, [], %{id: ""})
      %LiveFit.Fitness.Category{id: category_id} = category_fixture()
      %LiveFit.Fitness.Muscle{id: muscle_id} = muscle_fixture()

      {:ok, index_live, _html} = live(conn, Routes.workout_index_path(conn, :index))

      assert index_live
      |> form("#workout_search_form", search: @empty_search_attrs)
      |> render_change() =~ workout.title

      refute index_live
      |> form("#workout_search_form", search: %{@empty_search_attrs | search_phrase: "random"})
      |> render_change() =~ workout.title

      refute index_live
      |> form("#workout_search_form", search: %{@empty_search_attrs | category_id: category_id})
      |> render_change() =~ workout.title

      refute index_live
      |> form("#workout_search_form", search: %{@empty_search_attrs | muscle_ids: [muscle_id]})
      |> render_change() =~ workout.title

      refute index_live
      |> form("#workout_search_form", search: %{@empty_search_attrs | equipment_needed: false})
      |> render_change() =~ workout_with_equipment.title
    end

    test "saves new workout without category and muscles", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, Routes.workout_index_path(conn, :new))

      {:ok, _, html} =
        index_live
        |> form("#workout-form", workout: @create_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.workout_index_path(conn, :index))

      assert html =~ "Workout created successfully"
      assert html =~ @create_attrs.description
      assert html =~ @create_attrs.link
      assert html =~ @create_attrs.title
      assert html =~ @create_attrs.equipment_needed
    end

    test "saves new workout with category and muscles", %{conn: conn, category: category, muscles: muscles} do
      {:ok, index_live, _html} = live(conn, Routes.workout_index_path(conn, :index))

      assert index_live |> element("a", "New Workout") |> render_click() =~
               "New Workout"

      assert_patch(index_live, Routes.workout_index_path(conn, :new))

      assert index_live
             |> form("#workout-form", workout: @invalid_attrs)
             |> render_change() =~ "can&apos;t be blank"

      {:ok, _, html} =
        index_live
        |> form("#workout-form", workout: put_category_and_muscles_to_attributes(@create_attrs, category.id, muscle_ids(muscles)))
        |> render_submit()
        |> follow_redirect(conn, Routes.workout_index_path(conn, :index))

      assert html =~ "Workout created successfully"
      assert html =~ category.title
      Enum.each(muscles, fn muscle ->
        assert html =~ muscle.name
      end)
    end

    test "updates workout in listing", %{conn: conn, workout: workout, category: category, muscles: muscles} do
      {:ok, index_live, _html} = live(conn, Routes.workout_index_path(conn, :index))

      assert index_live |> element("#workout-#{workout.id} a", "Edit") |> render_click() =~
               "Edit Workout"

      assert_patch(index_live, Routes.workout_index_path(conn, :edit, workout))

      assert index_live
             |> form("#workout-form", workout: @invalid_attrs)
             |> render_change() =~ "can&apos;t be blank"

      {:ok, _, html} =
        index_live
        |> form("#workout-form", workout: @update_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.workout_index_path(conn, :index))

      assert html =~ "Workout updated successfully"
      assert html =~ @update_attrs.description
      assert html =~ @update_attrs.link
      assert html =~ @update_attrs.title
      assert html =~ @update_attrs.equipment_needed
      assert html =~ category.title
      Enum.each(muscles, fn muscle ->
        assert html =~ muscle.name
      end)
    end

    test "updates workout in listing and deselects all muscles and category", %{conn: conn, workout: workout, category: category, muscles: muscles} do
      {:ok, index_live, _html} = live(conn, Routes.workout_index_path(conn, :edit, workout))

      {:ok, second_index_live, html} =
        index_live
        |> form("#workout-form", workout: put_category_and_muscles_to_attributes(@update_attrs, "", ""))
        |> render_submit()
        |> follow_redirect(conn, Routes.workout_index_path(conn, :index))

      assert html =~ "Workout updated successfully"
      refute workouts_table(second_index_live) =~ category.title
      Enum.each(muscles, fn muscle ->
        refute workouts_table(second_index_live) =~ muscle.name
      end)
    end

    test "updates workout in listing and deselects one muscle", %{conn: conn, workout: workout, muscles: [first_muscle | rest_muscles]} do
      {:ok, index_live, _html} = live(conn, Routes.workout_index_path(conn, :edit, workout))

      {:ok, second_index_live, html} =
        index_live
        |> form("#workout-form", workout: put_category_and_muscles_to_attributes(@update_attrs, "", muscle_ids(rest_muscles)))
        |> render_submit()
        |> follow_redirect(conn, Routes.workout_index_path(conn, :index))

      assert html =~ "Workout updated successfully"
      refute workouts_table(second_index_live) =~ first_muscle.name
      Enum.each(rest_muscles, fn muscle ->
        assert workouts_table(second_index_live) =~ muscle.name
      end)
    end

    test "deletes workout in listing", %{conn: conn, workout: workout} do
      {:ok, index_live, _html} = live(conn, Routes.workout_index_path(conn, :index))

      assert index_live |> element("#workout-#{workout.id} a", "Delete") |> render_click()
      refute has_element?(index_live, "#workout-#{workout.id}")
    end
  end

  describe "Show" do
    setup [:create_workout_and_trainer]

    test "displays workout", %{conn: conn, workout: workout, category: category, muscles: muscles} do
      {:ok, show_live, html} = live(conn, Routes.workout_show_path(conn, :show, workout))

      assert html =~ "Show Workout"
      refute show_live |> element("main.container") |> render() =~ workout.user.email
      assert html =~ workout.description
      assert html =~ workout.link
      assert html =~ workout.title
      assert html =~ to_string(workout.equipment_needed)
      assert html =~ category.title
      Enum.each(muscles, fn muscle ->
        assert html =~ muscle.name
      end)
    end

    test "displays youtube workout video", %{conn: conn} do
      workout = workout_fixture(%{link: "www.youtube.com/myvid"}, trainer_fixture(), [], %{id: ""})
      {:ok, show_live, _html} = live(conn, Routes.workout_show_path(conn, :show, workout))

      assert show_live |> element("iframe") |> render() =~ workout.link
    end

    test "displays facebook workout video", %{conn: conn} do
      workout = workout_fixture(%{link: "www.facebook.com/myvid"}, trainer_fixture(), [], %{id: ""})
      {:ok, show_live, _html} = live(conn, Routes.workout_show_path(conn, :show, workout))

      assert show_live |> element("iframe") |> render() =~ workout.link
    end

    test "updates workout within modal", %{conn: conn, workout: workout, category: category, muscles: muscles} do
      {:ok, show_live, _html} = live(conn, Routes.workout_show_path(conn, :show, workout))

      assert show_live |> element("a", "Edit") |> render_click() =~
               "Edit Workout"

      assert_patch(show_live, Routes.workout_show_path(conn, :edit, workout))

      assert show_live
             |> form("#workout-form", workout: @invalid_attrs)
             |> render_change() =~ "can&apos;t be blank"

      {:ok, _, html} =
        show_live
        |> form("#workout-form", workout: @update_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.workout_show_path(conn, :show, workout))

      assert html =~ "Workout updated successfully"
      assert html =~ @update_attrs.description
      assert html =~ @update_attrs.link
      assert html =~ @update_attrs.title
      assert html =~ @update_attrs.equipment_needed
      assert html =~ category.title
      Enum.each(muscles, fn muscle ->
        assert html =~ muscle.name
      end)
    end
  end

  defp workouts_table(view) do
    view
    |> element("tbody#workouts")
    |> render()
  end

  defp get_index_button(view, workout_id, text), do: element(view, "tr#workout-#{workout_id} button", ~r/^#{text}/)
  defp get_show_button(view, text), do: element(view, "button", ~r/^#{text}/)

end
