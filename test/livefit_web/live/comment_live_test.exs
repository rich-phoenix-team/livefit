defmodule LiveFitWeb.CommentLiveTest do
  use LiveFitWeb.ConnCase, async: false

  import Phoenix.LiveViewTest
  import LiveFit.AccountsFixtures
  import LiveFit.FitnessFixtures
  import LiveFit.InteractionsFixtures

  alias LiveFit.Interactions

  @create_attrs %{body: "some body"}
  @reply_attrs %{body: "some reply"}
  @update_attrs %{body: "some updated body"}
  @invalid_attrs %{body: nil}

  defp create_workout_and_trainee(%{conn: conn}) do
    trainee = trainee_fixture()
    workout = workout_fixture(trainer_fixture())
    comment = comment_fixture(trainee, workout.id)
    %{conn: log_in_user(conn, trainee), current_user: trainee, workout_id: workout.id, comment: comment}
  end

  describe "Index" do
    setup [:create_workout_and_trainee]

    test "lists all comments for the specific workout", %{conn: conn, current_user: current_user, comment: comment, workout_id: workout_id} do
      workout2 = workout_fixture(trainer_fixture())
      other_comment = comment_fixture(current_user, workout2.id)
      {:ok, _show_live, html} = live_isolated(conn, LiveFitWeb.CommentLive.Index, session: %{"workout_id" => workout_id})

      assert html =~ "Comments"
      assert html =~ comment.body
      refute html =~ other_comment.body
    end

    test "saves new root comment", %{conn: conn, workout_id: workout_id} do
      {:ok, show_live, _html} = live_isolated(conn, LiveFitWeb.CommentLive.Index, session: %{"workout_id" => workout_id})

      show_live
        |> form("#comment-form-new", comment: @create_attrs)
        |> render_submit()

      assert render(show_live) =~ "some body"
    end

    test "saves new child comment", %{conn: conn, workout_id: workout_id, comment: comment} do
      {:ok, show_live, _html} = live_isolated(conn, LiveFitWeb.CommentLive.Index, session: %{"workout_id" => workout_id})

      show_live
        |> element("article#comment-#{comment.id} a", "reply")
        |> render_click()
        |> IO.inspect

      show_live
        |> form("#comment-form-#{comment.id}", comment: @reply_attrs)
        |> render_submit()

      assert render(show_live) =~ "some reply"
    end

    # test "updates comment in listing", %{conn: conn, comment: comment} do
    #   {:ok, index_live, _html} = live(conn, Routes.comment_index_path(conn, :index))

    #   assert index_live |> element("#comment-#{comment.id} a", "Edit") |> render_click() =~
    #            "Edit Comment"

    #   assert_patch(index_live, Routes.comment_index_path(conn, :edit, comment))

    #   assert index_live
    #          |> form("#comment-form", comment: @invalid_attrs)
    #          |> render_change() =~ "can&apos;t be blank"

    #   {:ok, _, html} =
    #     index_live
    #     |> form("#comment-form", comment: @update_attrs)
    #     |> render_submit()
    #     |> follow_redirect(conn, Routes.comment_index_path(conn, :index))

    #   assert html =~ "Comment updated successfully"
    #   assert html =~ "some updated body"
    # end

    # test "deletes comment in listing", %{conn: conn, comment: comment} do
    #   {:ok, index_live, _html} = live(conn, Routes.comment_index_path(conn, :index))

    #   assert index_live |> element("#comment-#{comment.id} a", "Delete") |> render_click()
    #   refute has_element?(index_live, "#comment-#{comment.id}")
    # end
  end

  # describe "Show" do
  #   setup [:create_comment]

  #   test "displays comment", %{conn: conn, comment: comment} do
  #     {:ok, _show_live, html} = live(conn, Routes.comment_show_path(conn, :show, comment))

  #     assert html =~ "Show Comment"
  #     assert html =~ comment.body
  #   end

  #   test "updates comment within modal", %{conn: conn, comment: comment} do
  #     {:ok, show_live, _html} = live(conn, Routes.comment_show_path(conn, :show, comment))

  #     assert show_live |> element("a", "Edit") |> render_click() =~
  #              "Edit Comment"

  #     assert_patch(show_live, Routes.comment_show_path(conn, :edit, comment))

  #     assert show_live
  #            |> form("#comment-form", comment: @invalid_attrs)
  #            |> render_change() =~ "can&apos;t be blank"

  #     {:ok, _, html} =
  #       show_live
  #       |> form("#comment-form", comment: @update_attrs)
  #       |> render_submit()
  #       |> follow_redirect(conn, Routes.comment_show_path(conn, :show, comment))

  #     assert html =~ "Comment updated successfully"
  #     assert html =~ "some updated body"
  #   end
  # end
end
