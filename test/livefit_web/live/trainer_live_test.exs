defmodule LiveFitWeb.TrainerLiveTest do
  use LiveFitWeb.ConnCase

  import Phoenix.LiveViewTest
  import LiveFit.AccountsFixtures

  alias LiveFit.Interactions

  defp create_trainer(_) do
    %{trainer: trainer_fixture()}
  end

  defp log_in_trainee(%{conn: conn}) do
    trainee = trainee_fixture()
    %{conn: log_in_user(conn, trainee), current_user: trainee}
  end

  defp log_in_admin(%{conn: conn}) do
    admin = admin_fixture()
    %{conn: log_in_user(conn, admin), current_user: admin}
  end

  describe "Index" do
    setup [:create_trainer, :log_in_trainee]

    test "lists all trainers", %{conn: conn, trainer: trainer, current_user: current_user} do
      {:ok, _index_live, html} = live(conn, Routes.trainer_index_path(conn, :index))

      trainer_workout_count = Interactions.get_trainer!(current_user, trainer.id).workout_count

      assert html =~ "Listing Trainers"
      assert html =~ trainer.email
      assert html =~ to_string(trainer_workout_count)
    end

    test "follows and unfollows trainers", %{conn: conn, trainer: trainer, current_user: current_user} do
      {:ok, index_live, html} = live(conn, Routes.trainer_index_path(conn, :index))

      assert html =~ "Listing Trainers"



      assert get_index_button(index_live, trainer.id, "follow") |> has_element?()
      refute get_index_button(index_live, trainer.id, "unfollow") |> has_element?()

      get_index_button(index_live, trainer.id, "follow") |> render_click()
      assert [trainer] == Interactions.list_followed_trainers(current_user)

      assert get_index_button(index_live, trainer.id, "unfollow") |> has_element?()
      refute get_index_button(index_live, trainer.id, "follow") |> has_element?()

      get_index_button(index_live, trainer.id, "unfollow") |> render_click()
      assert [] == Interactions.list_followed_trainers(current_user)

      assert get_index_button(index_live, trainer.id, "follow") |> has_element?()
      refute get_index_button(index_live, trainer.id, "unfollow") |> has_element?()
    end
  end

  describe "Admin Index" do
    setup [:create_trainer, :log_in_admin]

    test "admin doesnt see follow and unfollow buttons", %{conn: conn, trainer: trainer} do
      {:ok, index_live, html} = live(conn, Routes.trainer_index_path(conn, :index))

      assert html =~ "Listing Trainers"
      assert html =~ trainer.email

      refute get_index_button(index_live, trainer.id, "follow") |> has_element?()
      refute get_index_button(index_live, trainer.id, "unfollow") |> has_element?()
    end

  end

  describe "Show" do
    setup [:create_trainer, :log_in_trainee]

    test "displays trainer", %{conn: conn, trainer: trainer, current_user: current_user} do
      {:ok, _show_live, html} = live(conn, Routes.trainer_show_path(conn, :show, trainer))

      trainer_workout_count = Interactions.get_trainer!(current_user, trainer.id).workout_count

      assert html =~ "Show Trainer"
      assert html =~ trainer.email
      assert html =~ to_string(trainer_workout_count)
    end

    test "follows and unfollows trainer", %{conn: conn, trainer: trainer, current_user: current_user} do
      {:ok, show_live, html} = live(conn, Routes.trainer_show_path(conn, :show, trainer))

      assert html =~ "Show Trainer"

      assert get_show_button(show_live, "follow") |> has_element?()
      refute get_show_button(show_live, "unfollow") |> has_element?()

      get_show_button(show_live, "follow") |> render_click()
      assert [trainer] == Interactions.list_followed_trainers(current_user)

      assert get_show_button(show_live, "unfollow") |> has_element?()
      refute get_show_button(show_live, "follow") |> has_element?()

      get_show_button(show_live, "unfollow") |> render_click()
      assert [] == Interactions.list_followed_trainers(current_user)

      assert get_show_button(show_live, "follow") |> has_element?()
      refute get_show_button(show_live, "unfollow") |> has_element?()
    end

  end

  defp get_index_button(view, trainer_id, text), do: element(view, "tr#trainer-#{trainer_id} button", ~r/^#{text}/)
  defp get_show_button(view, text), do: element(view, "button", ~r/^#{text}/)
end
