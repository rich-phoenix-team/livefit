defmodule LiveFitWeb.MuscleLiveTest do
  use LiveFitWeb.ConnCase

  import Phoenix.LiveViewTest
  import LiveFit.AccountsFixtures
  import LiveFit.FitnessFixtures

  @create_attrs %{name: "some name"}
  @update_attrs %{name: "some updated name"}
  @invalid_attrs %{name: nil}

  defp create_muscle(_) do
    %{muscle: muscle_fixture()}
  end

  defp log_in_admin(%{conn: conn}) do
    %{conn: log_in_user(conn, admin_fixture())}
  end

  describe "Index" do
    setup [:create_muscle, :log_in_admin]

    test "lists all muscles", %{conn: conn, muscle: muscle} do
      {:ok, _index_live, html} = live(conn, Routes.muscle_index_path(conn, :index))

      assert html =~ "Listing Muscles"
      assert html =~ muscle.name
    end

    test "saves new muscle", %{conn: conn, muscle: muscle} do
      {:ok, index_live, _html} = live(conn, Routes.muscle_index_path(conn, :index))

      assert index_live |> element("a", "New Muscle") |> render_click() =~
               "New Muscle"

      assert_patch(index_live, Routes.muscle_index_path(conn, :new))

      assert index_live
             |> form("#muscle-form", muscle: %{name: ""})
             |> render_change() =~ "can&apos;t be blank"

      assert index_live
             |> form("#muscle-form", muscle: %{name: muscle.name})
             |> render_submit() =~ "has already been taken"

      {:ok, _, html} =
        index_live
        |> form("#muscle-form", muscle: @create_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.muscle_index_path(conn, :index))

      assert html =~ "Muscle created successfully"
      assert html =~ "some name"
    end

    test "updates muscle in listing", %{conn: conn, muscle: muscle} do
      {:ok, index_live, _html} = live(conn, Routes.muscle_index_path(conn, :index))

      assert index_live |> element("#muscle-#{muscle.id} a", "Edit") |> render_click() =~
               "Edit Muscle"

      assert_patch(index_live, Routes.muscle_index_path(conn, :edit, muscle))

      assert index_live
             |> form("#muscle-form", muscle: @invalid_attrs)
             |> render_change() =~ "can&apos;t be blank"

      {:ok, _, html} =
        index_live
        |> form("#muscle-form", muscle: @update_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.muscle_index_path(conn, :index))

      assert html =~ "Muscle updated successfully"
      assert html =~ "some updated name"
    end

    test "deletes muscle in listing", %{conn: conn, muscle: muscle} do
      {:ok, index_live, _html} = live(conn, Routes.muscle_index_path(conn, :index))

      workout_fixture(trainer_fixture(), muscle, %{id: ""})

      assert index_live |> element("#muscle-#{muscle.id} a", "Delete") |> render_click()
      refute has_element?(index_live, "#muscle-#{muscle.id}")
    end
  end

  describe "Show" do
    setup [:create_muscle, :log_in_admin]

    test "displays muscle", %{conn: conn, muscle: muscle} do
      {:ok, _show_live, html} = live(conn, Routes.muscle_show_path(conn, :show, muscle))

      assert html =~ "Show Muscle"
      assert html =~ muscle.name
    end

    test "updates muscle within modal", %{conn: conn, muscle: muscle} do
      {:ok, show_live, _html} = live(conn, Routes.muscle_show_path(conn, :show, muscle))

      assert show_live |> element("a", "Edit") |> render_click() =~
               "Edit Muscle"

      assert_patch(show_live, Routes.muscle_show_path(conn, :edit, muscle))

      assert show_live
             |> form("#muscle-form", muscle: @invalid_attrs)
             |> render_change() =~ "can&apos;t be blank"

      {:ok, _, html} =
        show_live
        |> form("#muscle-form", muscle: @update_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.muscle_show_path(conn, :show, muscle))

      assert html =~ "Muscle updated successfully"
      assert html =~ "some updated name"
    end
  end
end
