defmodule LiveFit.FitnessTest do
  use LiveFit.DataCase

  alias LiveFit.Accounts.User
  alias LiveFit.Fitness
  alias LiveFit.Interactions
  import LiveFit.AccountsFixtures
  import LiveFit.FitnessFixtures
    alias LiveFit.Fitness.{Workout, Muscle, Category}

  describe "workouts" do

    @valid_attrs %{description: "some description", link: "some link", title: "some title"}
    @update_attrs %{description: "some updated description", link: "some updated link", title: "some updated title"}
    @invalid_attrs %{description: nil, link: nil, title: nil}

    test "list_workouts/0 returns all workouts" do
      %Workout{id: id} = workout_fixture(trainer_fixture())
      assert [%Workout{id: ^id}] = Fitness.list_workouts()
    end

    test "list_user_workouts/1 returns all workouts created by specific user" do
      trainer_1 = %User{id: trainer_1_id} = trainer_fixture()
      trainer_2 = trainer_fixture()
      %Workout{id: id_1} = workout_fixture(trainer_1)
      workout_fixture(trainer_2)
      assert [%Workout{id: ^id_1, user_id: ^trainer_1_id}] = Fitness.list_user_workouts(trainer_1)
    end

    test "list_user_workouts/1 returns workouts with is_favorite: true if they are some user's favorite" do
      %Workout{id: workout_id} = workout_fixture(trainer_fixture())
      trainee = trainee_fixture()
      assert {:ok, _} = Interactions.add_favorite_workout(trainee, workout_id)
      assert [%Workout{id: ^workout_id, is_favorite: true}] = Fitness.list_user_workouts(trainee)
    end

    test "search_workouts/2 returns workouts filtered by muscles" do
      trainer = trainer_fixture()
      muscle_arms = %Muscle{id: muscle_arms_id} = muscle_fixture(%{name: "arms"})
      muscle_legs = %Muscle{id: muscle_legs_id} = muscle_fixture(%{name: "legs"})
      workout_1 = workout_fixture(trainer, [muscle_arms], %{id: ""})
      workout_2 = workout_fixture(trainer, [muscle_legs], %{id: ""})
      workout_3 = workout_fixture(trainer, [muscle_arms, muscle_legs], %{id: ""})
      workout_4 = workout_fixture(trainer, [], %{id: ""})
      assert equal_list([workout_1, workout_3], Fitness.search_workouts(trainer, %{muscle_ids: [muscle_arms_id]}))
      assert equal_list([workout_2, workout_3], Fitness.search_workouts(trainer, %{muscle_ids: [muscle_legs_id]}))
      assert equal_list([workout_3], Fitness.search_workouts(trainer, %{muscle_ids: [muscle_arms_id, muscle_legs_id]}))
      assert equal_list([workout_1, workout_2, workout_3, workout_4], Fitness.search_workouts(trainer, %{muscle_ids: []}))
    end

    test "search_workouts/2 returns workouts filtered by category" do
      trainer = trainer_fixture()
      category_strength = %Category{} = category_fixture(%{title: "Strength"})
      category_cardio = %Category{} = category_fixture(%{title: "Cardio"})
      workout_1 = workout_fixture(trainer, [], category_strength)
      workout_2 = workout_fixture(trainer, [], category_cardio)
      workout_3 = workout_fixture(trainer, [], %{id: ""})
      assert equal_list([workout_1], Fitness.search_workouts(trainer, %{category_id: category_strength.id}))
      assert equal_list([workout_2], Fitness.search_workouts(trainer, %{category_id: category_cardio.id}))
      assert equal_list([workout_1, workout_2, workout_3], Fitness.search_workouts(trainer, %{category_id: nil}))
    end

    test "search_workouts/2 returns workouts filtered by title" do
      trainer = trainer_fixture()
      workout_1 = workout_fixture(%{title: "workout"}, trainer, [], %{id: ""})
      workout_2 = workout_fixture(%{title: "fitness"}, trainer, [], %{id: ""})
      assert equal_list([workout_1], Fitness.search_workouts(trainer, %{search_phrase: "work"}))
      assert equal_list([workout_1, workout_2], Fitness.search_workouts(trainer, %{search_phrase: ""}))
    end

    test "search_workouts/2 returns workouts filtered by equipment needed" do
      trainer = trainer_fixture()
      workout_1 = workout_fixture(%{equipment_needed: false}, trainer, [], %{id: ""})
      workout_2 = workout_fixture(%{equipment_needed: true}, trainer, [], %{id: ""})
      assert equal_list([workout_1], Fitness.search_workouts(trainer, %{equipment_needed: false}))
      assert equal_list([workout_1, workout_2], Fitness.search_workouts(trainer, %{equipment_needed: true}))
    end

    test "search_workouts/2 returns workouts only for the right user" do
      trainer_1 = trainer_fixture()
      trainer_2 = trainer_fixture()
      workout_1 = workout_fixture(trainer_1)
      workout_2 = workout_fixture(trainer_2)
      assert equal_list([workout_1], Fitness.search_workouts(trainer_1, %{}))
      assert equal_list([workout_1, workout_2], Fitness.search_workouts(trainee_fixture(), %{}))
    end

    test "search_workouts/2 returns correctly filtered workouts" do
      muscle_arms = %Muscle{id: muscle_arms_id} = muscle_fixture(%{name: "arms"})
      muscle_legs = %Muscle{id: muscle_legs_id} = muscle_fixture(%{name: "legs"})
      category_strength = %Category{} = category_fixture(%{title: "Strength"})
      category_cardio = %Category{id: category_cardio_id} = category_fixture(%{title: "Cardio"})
      trainer_1 = %User{} = trainer_fixture()
      trainer_2 = trainer_fixture()

      workout_1 = workout_fixture(%{equipment_needed: false, title: "gen workout"}, trainer_1, [], %{id: ""})
      workout_2 = workout_fixture(%{equipment_needed: true, title: "strength workout"}, trainer_1, [], category_strength)
      workout_3 = workout_fixture(%{equipment_needed: false, title: "my workout"}, trainer_1, [muscle_legs], category_cardio)
      workout_4 = workout_fixture(%{equipment_needed: false, title: "load fitness"}, trainer_1, [muscle_legs, muscle_arms], category_strength)
      workout_5 = workout_fixture(%{equipment_needed: true, title: "nice"}, trainer_1, [muscle_arms], %{id: ""})
      workout_6 = workout_fixture(%{equipment_needed: false, title: "whatever"}, trainer_2, [], %{id: ""})

      assert equal_list([workout_1, workout_2, workout_3, workout_4, workout_5], Fitness.search_workouts(trainer_1, %{}))
      assert equal_list([workout_1, workout_3, workout_4], Fitness.search_workouts(trainer_1, %{equipment_needed: false}))
      assert equal_list([workout_1, workout_3], Fitness.search_workouts(trainer_1, %{equipment_needed: false, search_phrase: "work"}))
      assert equal_list([workout_3, workout_4], Fitness.search_workouts(trainer_1, %{equipment_needed: false, muscle_ids: [muscle_legs_id]}))
      assert equal_list([workout_3], Fitness.search_workouts(trainer_1, %{equipment_needed: false, muscle_ids: [muscle_legs_id], category_id: category_cardio_id}))

      assert equal_list([workout_4], Fitness.search_workouts(trainer_1, %{equipment_needed: true, muscle_ids: [muscle_legs_id, muscle_arms_id]}))


      assert equal_list([workout_6], Fitness.search_workouts(trainer_2, %{}))
      assert equal_list([], Fitness.search_workouts(trainer_2, %{search_phrase: "work"}))
    end

    test "get_workout!/2 returns the workout with given id" do
      trainer = trainer_fixture()
      %Workout{id: id} = workout_fixture(trainer)
      assert %Workout{id: ^id} = Fitness.get_workout!(trainer, id)
    end

    test "get_workout!/2 returns workouts with is_favorite: true if they are some user's favorite" do
      %Workout{id: workout_id} = workout_fixture(trainer_fixture())
      trainee = trainee_fixture()
      assert {:ok, _} = Interactions.add_favorite_workout(trainee, workout_id)
      assert %Workout{id: ^workout_id, is_favorite: true} = Fitness.get_workout!(trainee, workout_id)
    end

    test "new_workout/0 returns a new workout with preloaded muscles" do
      assert %Workout{muscles: []} = Fitness.new_workout()
    end

    test "create_workout/2 with valid data creates a workout" do
      %LiveFit.Accounts.User{id: user_id} = trainer = trainer_fixture()
      assert {:ok, %Workout{} = workout} = Fitness.create_workout(@valid_attrs, trainer)
      assert workout.description == "some description"
      assert workout.link == "some link"
      assert workout.title == "some title"
      assert workout.user_id == user_id
    end

    test "create_workout/2 with invalid data returns error changeset" do
      assert {:error, changeset = %Ecto.Changeset{}} = Fitness.create_workout(@invalid_attrs, trainer_fixture())

      assert %{
        title: ["can't be blank"],
        link: ["can't be blank"]
      } = errors_on(changeset)
    end

    test "update_workout/2 with valid data updates the workout" do
      workout = workout_fixture(trainer_fixture())
      assert {:ok, %Workout{} = workout} = Fitness.update_workout(workout, @update_attrs)
      assert workout.description == "some updated description"
      assert workout.link == "some updated link"
      assert workout.title == "some updated title"
    end

    test "update_workout/2 with invalid data returns error changeset" do
      trainer = trainer_fixture()
      %Workout{id: id} = workout = workout_fixture(trainer)
      assert {:error, %Ecto.Changeset{} = changeset} = Fitness.update_workout(workout, @invalid_attrs)
      assert %Workout{id: ^id} = Fitness.get_workout!(trainer, id)

      assert %{
        title: ["can't be blank"],
        link: ["can't be blank"]
      } = errors_on(changeset)
    end

    test "delete_workout/1 deletes the workout and like/views/favorites" do
      trainer = trainer_fixture()
      trainee = trainee_fixture()
      workout = workout_fixture(trainer)
      assert {:ok, %{id: like_id}} = LiveFit.Interactions.like_workout(trainee, workout.id)
      assert {:ok, %{id: view_id}} = LiveFit.Interactions.add_workout_view(trainee, workout.id)
      assert {:ok, %{id: favorite_id}} = LiveFit.Interactions.add_favorite_workout(trainee, workout.id)

      assert {:ok, %Workout{}} = Fitness.delete_workout(workout)
      assert_raise Ecto.NoResultsError, fn -> Fitness.get_workout!(trainer, workout.id) end

      assert_raise Ecto.NoResultsError, fn -> LiveFit.Repo.get!(LiveFit.Interactions.WorkoutLike, like_id) end
      assert_raise Ecto.NoResultsError, fn -> LiveFit.Repo.get!(LiveFit.Interactions.WorkoutView, view_id) end
      assert_raise Ecto.NoResultsError, fn -> LiveFit.Repo.get!(LiveFit.Interactions.FavoriteWorkout, favorite_id) end
    end

    test "change_workout/1 returns a workout changeset" do
      workout = workout_fixture(trainer_fixture())
      assert %Ecto.Changeset{} = Fitness.change_workout(workout)
    end
  end

  describe "categories" do

    @valid_attrs %{title: "some title"}
    @update_attrs %{title: "some updated title"}
    @invalid_attrs %{title: nil}

    test "list_categories/0 returns all categories" do
      category = category_fixture()
      assert Fitness.list_categories() == [category]
    end

    test "get_category!/1 returns the category with given id" do
      category = category_fixture()
      assert Fitness.get_category!(category.id) == category
    end

    test "create_category/1 with valid data creates a category" do
      assert {:ok, %Category{} = category} = Fitness.create_category(@valid_attrs)
      assert category.title == "some title"
    end

    test "create_category/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Fitness.create_category(@invalid_attrs)
    end

    test "update_category/2 with valid data updates the category" do
      category = category_fixture()
      assert {:ok, %Category{} = category} = Fitness.update_category(category, @update_attrs)
      assert category.title == "some updated title"
    end

    test "update_category/2 with invalid data returns error changeset" do
      category = category_fixture()
      assert {:error, %Ecto.Changeset{}} = Fitness.update_category(category, @invalid_attrs)
      assert category == Fitness.get_category!(category.id)
    end

    test "delete_category/1 deletes the category" do
      category = category_fixture()
      trainer = trainer_fixture()
      workout = workout_fixture(trainer, [], category)
      assert workout.category_id == category.id

      assert {:ok, %Category{}} = Fitness.delete_category(category)
      assert_raise Ecto.NoResultsError, fn -> Fitness.get_category!(category.id) end
      assert Fitness.get_workout!(trainer, workout.id).category_id == nil
    end

    test "change_category/1 returns a category changeset" do
      category = category_fixture()
      assert %Ecto.Changeset{} = Fitness.change_category(category)
    end
  end

  describe "muscles" do

    @valid_attrs %{name: "some name"}
    @update_attrs %{name: "some updated name"}
    @invalid_attrs %{name: nil}

    test "list_muscles/0 returns all muscles" do
      muscle = muscle_fixture()
      assert Fitness.list_muscles() == [muscle]
    end

    test "get_muscles_by_id/1 returns all muscles with that id" do
      muscle_1 = %Muscle{id: id_1} = muscle_fixture(%{name: "m1"})
      muscle_fixture(%{name: "m2"})
      muscle_3 = %Muscle{id: id_3} = muscle_fixture(%{name: "m3"})
      assert Fitness.get_muscles_by_id([id_1, id_3]) == [muscle_1, muscle_3]
    end

    test "get_muscles_by_id/1 with empty values returns no muscles" do
      muscle_fixture()
      assert Fitness.get_muscles_by_id([]) == []
      assert Fitness.get_muscles_by_id("") == []
    end

    test "get_muscle!/1 returns the muscle with given id" do
      muscle = muscle_fixture()
      assert Fitness.get_muscle!(muscle.id) == muscle
    end

    test "create_muscle/1 with valid data creates a muscle" do
      assert {:ok, %Muscle{} = muscle} = Fitness.create_muscle(@valid_attrs)
      assert muscle.name == "some name"
    end

    test "create_muscle/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Fitness.create_muscle(@invalid_attrs)
    end

    test "update_muscle/2 with valid data updates the muscle" do
      muscle = muscle_fixture()
      assert {:ok, %Muscle{} = muscle} = Fitness.update_muscle(muscle, @update_attrs)
      assert muscle.name == "some updated name"
    end

    test "update_muscle/2 with invalid data returns error changeset" do
      muscle = muscle_fixture()
      assert {:error, %Ecto.Changeset{}} = Fitness.update_muscle(muscle, @invalid_attrs)
      assert muscle == Fitness.get_muscle!(muscle.id)
    end

    test "delete_muscle/1 deletes the muscle" do
      trainer = trainer_fixture()
      muscle = muscle_fixture()
      workout = workout_fixture(trainer, muscle, %{id: ""})
      assert Fitness.get_workout!(trainer, workout.id).muscles == [muscle]

      assert {:ok, %Muscle{}} = Fitness.delete_muscle(muscle)
      assert_raise Ecto.NoResultsError, fn -> Fitness.get_muscle!(muscle.id) end
      assert Fitness.get_workout!(trainer, workout.id).muscles == []
    end

    test "change_muscle/1 returns a muscle changeset" do
      muscle = muscle_fixture()
      assert %Ecto.Changeset{} = Fitness.change_muscle(muscle)
    end
  end
end
