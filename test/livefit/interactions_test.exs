defmodule LiveFit.InteractionsTest do
  use LiveFit.DataCase

  alias LiveFit.Interactions
  alias LiveFit.Interactions.{UserFollowing, FavoriteWorkout, WorkoutView, WorkoutLike, Comment}
  alias LiveFit.Accounts.User
  alias LiveFit.Fitness.Workout
  import LiveFit.AccountsFixtures
  import LiveFit.FitnessFixtures
  import LiveFit.InteractionsFixtures

  describe "trainers" do

    setup do
      %{trainer: trainer_fixture(), trainee: trainee_fixture()}
    end

    test "list_trainers/1 returns all trainers", %{trainer: %User{id: trainer_id}} do
      assert [%User{role: :trainer, id: ^trainer_id}] = Interactions.list_trainers(%{id: trainer_id})
    end

    test "get_trainer!/2 returns the trainer with given id", %{trainer: %User{id: trainer_id}} do
      assert %User{role: :trainer, id: ^trainer_id} = Interactions.get_trainer!(%{id: trainer_id}, trainer_id)
    end

    test "follow/2 allows trainee to follow trainer once", %{trainer: %User{id: trainer_id} = trainer, trainee: %User{id: trainee_id} = trainee} do
      assert {:ok, %UserFollowing{follower_id: ^trainee_id, followed_id: ^trainer_id}} = Interactions.follow(trainee, trainer_id)
      assert {:ok, %UserFollowing{follower_id: ^trainee_id, followed_id: ^trainer_id}} = Interactions.follow(trainee, trainer_id)
      assert [trainer] == Interactions.list_followed_trainers(trainee)
    end

    test "unfollow/2 removes trainer from followed", %{trainer: %User{id: trainer_id} = trainer, trainee: %User{id: trainee_id} = trainee} do
      assert {:ok, %UserFollowing{follower_id: ^trainee_id, followed_id: ^trainer_id}} = Interactions.follow(trainee, trainer_id)
      assert [trainer] == Interactions.list_followed_trainers(trainee)
      assert {1, _} = Interactions.unfollow(trainee, trainer_id)
      assert [] == Interactions.list_followed_trainers(trainee)
      assert {0, _} = Interactions.unfollow(trainee, trainer_id)
      assert [] == Interactions.list_followed_trainers(trainee)
    end

    test "list_followed_trainers/1 returns followed trainers", %{trainer: trainer, trainee: trainee} do
      trainer2 = trainer_fixture()

      assert {:ok, _} = Interactions.follow(trainee, trainer.id)
      assert [trainer] == Interactions.list_followed_trainers(trainee)
      assert {:ok, _} = Interactions.follow(trainee, trainer2.id)
      assert equal_list([trainer, trainer2], Interactions.list_followed_trainers(trainee))
    end

    test "list_trainers/1 returns trainers that have is_followed: true if they are followed", %{trainer: %User{id: trainer_id}, trainee: trainee} do
      assert {:ok, _} = Interactions.follow(trainee, trainer_id)
      assert [%User{id: ^trainer_id, is_followed: true}] = Interactions.list_trainers(trainee)
    end

    test "get_trainer!/2 returns trainer that have is_followed: true if they are followed", %{trainer: %User{id: trainer_id}, trainee: trainee} do
      assert {:ok, _} = Interactions.follow(trainee, trainer_id)
      assert %User{id: ^trainer_id, is_followed: true} = Interactions.get_trainer!(trainee, trainer_id)
    end
  end

  describe "workouts" do

    setup do
      trainer = trainer_fixture()
      trainee = trainee_fixture()
      workout = workout_fixture(trainer)
      %{trainer: trainer, workout: workout, trainee: trainee}
    end

    test "list_favorite_workouts/1 returns all user's favorite workouts", %{workout: %{id: workout_id}, trainee: trainee, trainer: trainer} do
      %{id: workout2_id} = workout_fixture(trainer)

      assert {:ok, _} = Interactions.add_favorite_workout(trainee, workout_id)
      assert [%Workout{id: ^workout_id}] = Interactions.list_favorite_workouts(trainee)
      assert {:ok, _} = Interactions.add_favorite_workout(trainee, workout2_id)
      assert equal_list([%Workout{id: workout_id}, %Workout{id: workout2_id}], Interactions.list_favorite_workouts(trainee))
    end

    test "add_favorite_workout/2 allows trainee to follow workout once", %{workout: %{id: workout_id}, trainee: %User{id: trainee_id} = trainee} do
      assert {:ok, %FavoriteWorkout{user_id: ^trainee_id, workout_id: ^workout_id}} = Interactions.add_favorite_workout(trainee, workout_id)
      assert {:ok, %FavoriteWorkout{user_id: ^trainee_id, workout_id: ^workout_id}} = Interactions.add_favorite_workout(trainee, workout_id)
      assert [%Workout{id: ^workout_id}] = Interactions.list_favorite_workouts(trainee)
    end

    test "remove_favorite_workout/2 removes workout from user's favorites", %{workout: %{id: workout_id}, trainee: %User{id: trainee_id} = trainee} do
      assert {:ok, %FavoriteWorkout{user_id: ^trainee_id, workout_id: ^workout_id}} = Interactions.add_favorite_workout(trainee, workout_id)
      assert [%Workout{id: ^workout_id}] = Interactions.list_favorite_workouts(trainee)
      assert {1, _} = Interactions.remove_favorite_workout(trainee, workout_id)
      assert [] == Interactions.list_favorite_workouts(trainee)
      assert {0, _} = Interactions.remove_favorite_workout(trainee, workout_id)
      assert [] == Interactions.list_favorite_workouts(trainee)
    end

    test "list_liked_workouts/1 returns all user's liked workouts", %{workout: %{id: workout_id}, trainee: trainee, trainer: trainer} do
      %{id: workout2_id} = workout_fixture(trainer)

      assert {:ok, _} = Interactions.like_workout(trainee, workout_id)
      assert [%Workout{id: ^workout_id}] = Interactions.list_liked_workouts(trainee)
      assert {:ok, _} = Interactions.like_workout(trainee, workout2_id)
      assert equal_list([%Workout{id: workout_id}, %Workout{id: workout2_id}], Interactions.list_liked_workouts(trainee))
    end

    test "get_like_count/1 returns how many likes a workout has", %{workout: %{id: workout_id}, trainee: %User{id: trainee_id} = trainee} do
      trainee2 = %{id: trainee2_id} = trainee_fixture()

      assert 0 == Interactions.get_like_count(workout_id)
      assert {:ok, %WorkoutLike{user_id: ^trainee_id, workout_id: ^workout_id}} = Interactions.like_workout(trainee, workout_id)
      assert 1 == Interactions.get_like_count(workout_id)
      assert {:ok, %WorkoutLike{user_id: ^trainee2_id, workout_id: ^workout_id}} = Interactions.like_workout(trainee2, workout_id)
      assert 2 == Interactions.get_like_count(workout_id)
    end

    test "like_workout/2 allows trainee to follow workout once", %{workout: %{id: workout_id}, trainee: %User{id: trainee_id} = trainee} do
      assert {:ok, %WorkoutLike{user_id: ^trainee_id, workout_id: ^workout_id}} = Interactions.like_workout(trainee, workout_id)
      assert {:ok, %WorkoutLike{user_id: ^trainee_id, workout_id: ^workout_id}} = Interactions.like_workout(trainee, workout_id)
      assert [%Workout{id: ^workout_id}] = Interactions.list_liked_workouts(trainee)
    end

    test "unlike_workout/2 removes workout from user's favorites", %{workout: %{id: workout_id}, trainee: %User{id: trainee_id} = trainee} do
      assert {:ok, %WorkoutLike{user_id: ^trainee_id, workout_id: ^workout_id}} = Interactions.like_workout(trainee, workout_id)
      assert [%Workout{id: ^workout_id}] = Interactions.list_liked_workouts(trainee)
      assert {1, _} = Interactions.unlike_workout(trainee, workout_id)
      assert [] == Interactions.list_favorite_workouts(trainee)
      assert {0, _} = Interactions.unlike_workout(trainee, workout_id)
      assert [] == Interactions.list_favorite_workouts(trainee)
    end

    test "list_viewed_workouts/1 returns all user's viewed workouts", %{workout: %{id: workout_id}, trainee: trainee, trainer: trainer} do
      %{id: workout2_id} = workout_fixture(trainer)

      assert {:ok, _} = Interactions.add_workout_view(trainee, workout_id)
      assert [%Workout{id: ^workout_id}] = Interactions.list_viewed_workouts(trainee)
      assert {:ok, _} = Interactions.add_workout_view(trainee, workout2_id)
      assert equal_list([%Workout{id: workout_id}, %Workout{id: workout2_id}], Interactions.list_viewed_workouts(trainee))
    end

    test "get_view_count/1 returns how many likes a workout has", %{workout: %{id: workout_id}, trainee: %User{id: trainee_id} = trainee} do
      trainee2 = %{id: trainee2_id} = trainee_fixture()

      assert 0 == Interactions.get_view_count(workout_id)
      assert {:ok, %WorkoutView{user_id: ^trainee_id, workout_id: ^workout_id}} = Interactions.add_workout_view(trainee, workout_id)
      assert 1 == Interactions.get_view_count(workout_id)
      assert {:ok, %WorkoutView{user_id: ^trainee2_id, workout_id: ^workout_id}} = Interactions.add_workout_view(trainee2, workout_id)
      assert 2 == Interactions.get_view_count(workout_id)
    end

    test "add_workout_view/2 allows trainee to follow workout once", %{workout: %{id: workout_id}, trainee: %User{id: trainee_id} = trainee} do
      assert {:ok, %WorkoutView{user_id: ^trainee_id, workout_id: ^workout_id}} = Interactions.add_workout_view(trainee, workout_id)
      assert {:ok, %WorkoutView{user_id: ^trainee_id, workout_id: ^workout_id}} = Interactions.add_workout_view(trainee, workout_id)
      assert 1 == Interactions.get_view_count(workout_id)
      assert [%Workout{id: ^workout_id}] = Interactions.list_viewed_workouts(trainee)
    end

  end


  describe "comments" do

    @valid_attrs %{body: "some body"}
    @update_attrs %{body: "some updated body"}
    @invalid_attrs %{body: nil}

    setup do
      trainer = trainer_fixture()
      workout = workout_fixture(trainer)
      comment = comment_fixture(trainer, workout.id)
      %{comment: comment, workout: workout, trainer: trainer}
    end

    test "list_comments/0 returns all comments", %{comment: %Comment{id: comment_id}} do
      assert [%Comment{id: ^comment_id }] = Interactions.list_comments()
    end

    test "list_root_comments/1 returns all root comments for specific workout", %{comment: %Comment{id: comment_id}, trainer: trainer, workout: workout} do
      comment_fixture(%{parent_id: comment_id}, trainer, workout.id)
      assert [%Comment{id: ^comment_id }] = Interactions.list_root_comments(workout.id)
    end

    test "get_child_comments/1 returns all child comments for specific comment", %{comment: %Comment{id: comment_id}, trainer: trainer, workout: workout} do
      %Comment{id: child_id} = comment_fixture(%{parent_id: comment_id}, trainer, workout.id)
      assert %{^comment_id => [%Comment{id: ^child_id }]} = Interactions.get_child_comments([comment_id])
    end

    test "get_comment!/1 returns the comment with given id", %{comment: %Comment{id: comment_id}} do
      assert %Comment{id: ^comment_id } = Interactions.get_comment!(comment_id)
    end

    test "create_comment/2 with valid data creates a comment", %{workout: workout, trainer: trainer} do
      attrs_with_workout_id = Map.put(@valid_attrs, :workout_id, workout.id)
      assert {:ok, %Comment{} = comment} = Interactions.create_comment(attrs_with_workout_id, trainer)
      assert comment.body == "some body"
      assert comment.workout_id == workout.id
      assert comment.user_id == trainer.id
    end

    test "create_comment/1 with invalid data returns error changeset", %{trainer: trainer} do
      assert {:error, %Ecto.Changeset{} = changeset} = Interactions.create_comment(@invalid_attrs, trainer)
      assert %{
        body: ["can't be blank"]
      } = errors_on(changeset)
    end

    test "update_comment/2 with valid data updates the comment", %{comment: comment} do
      assert {:ok, %Comment{} = comment} = Interactions.update_comment(comment, @update_attrs)
      assert comment.body == "some updated body"
    end

    test "update_comment/2 with invalid data returns error changeset", %{comment: %Comment{id: comment_id} = comment} do
      assert {:error, %Ecto.Changeset{} = changeset} = Interactions.update_comment(comment, @invalid_attrs)
      assert %Comment{id: ^comment_id} = Interactions.get_comment!(comment.id)
      assert %{
        body: ["can't be blank"]
      } = errors_on(changeset)
    end

    test "delete_comment/1 doesnt delete the comment, but instead sets deleted_at timestamp", %{comment: %Comment{id: comment_id} = comment} do
      assert {:ok, %Comment{}} = Interactions.delete_comment(comment)
      assert %Comment{id: ^comment_id, deleted_at: deleted_at} = Interactions.get_comment!(comment.id)
      assert deleted_at != nil
    end

    test "change_comment/1 returns a comment changeset", %{comment: comment} do
      assert %Ecto.Changeset{} = Interactions.change_comment(comment)
    end
  end
end
