defmodule LiveFit.Repo.Migrations.AddEquipmentNeededToWorkout do
  use Ecto.Migration

  def change do
    alter table(:workouts) do
      add :equipment_needed, :boolean, null: false, default: false
    end
  end
end
