defmodule LiveFit.Repo.Migrations.AddUserToWorkouts do
  use Ecto.Migration

  def change do
    alter table(:workouts) do
      add :user_id, references(:users, on_delete: :delete_all), null: false
    end

    create index(:workouts, [:user_id])
  end
end
