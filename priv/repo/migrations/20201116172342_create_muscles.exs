defmodule LiveFit.Repo.Migrations.CreateMuscles do
  use Ecto.Migration

  def change do
    create table(:muscles) do
      add :name, :string, null: false

      timestamps()
    end

    create unique_index(:muscles, [:name])
  end
end
