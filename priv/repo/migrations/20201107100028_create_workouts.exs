defmodule LiveFit.Repo.Migrations.CreateWorkouts do
  use Ecto.Migration

  def change do
    create table(:workouts) do
      add :title, :string, null: false
      add :link, :string, null: false
      add :description, :text

      timestamps()
    end

  end
end
