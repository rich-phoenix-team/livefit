defmodule LiveFit.Repo.Migrations.CreateJoinTableWorkoutLikes do
  use Ecto.Migration

  def change do
    create table(:workout_likes) do
      add :user_id, references(:users, on_delete: :delete_all)
      add :workout_id, references(:workouts, on_delete: :delete_all)
    end

    create unique_index(:workout_likes, [:user_id, :workout_id])
  end
end
