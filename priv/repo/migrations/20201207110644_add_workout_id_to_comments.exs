defmodule LiveFit.Repo.Migrations.AddWorkoutIdToComments do
  use Ecto.Migration

  def change do
    alter table(:comments) do
      add :workout_id, references(:workouts, on_delete: :delete_all), null: false
    end

    create index(:comments, [:workout_id])
  end
end
