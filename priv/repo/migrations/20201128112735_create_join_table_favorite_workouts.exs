defmodule LiveFit.Repo.Migrations.CreateJoinTableFavoriteWorkouts do
  use Ecto.Migration

  def change do
    create table(:favorite_workouts) do
      add :user_id, references(:users, on_delete: :delete_all)
      add :workout_id, references(:workouts, on_delete: :delete_all)
    end

    create unique_index(:favorite_workouts, [:user_id, :workout_id])
  end
end
