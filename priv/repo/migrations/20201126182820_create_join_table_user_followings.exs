defmodule LiveFit.Repo.Migrations.CreateJoinTableUserFollowings do
  use Ecto.Migration

  def change do
    create table(:user_followings) do
      add :follower_id, references(:users, on_delete: :delete_all)
      add :followed_id, references(:users, on_delete: :delete_all)
    end

    create unique_index(:user_followings, [:follower_id, :followed_id])
  end
end
