defmodule LiveFit.Repo.Migrations.AddCategoryToWorkout do
  use Ecto.Migration

  def change do
    alter table(:workouts) do
      add :category_id, references(:categories, on_delete: :nilify_all)
    end

    create index(:workouts, [:category_id])
  end
end
