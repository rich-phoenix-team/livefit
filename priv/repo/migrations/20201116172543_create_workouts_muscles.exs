defmodule LiveFit.Repo.Migrations.CreateWorkoutsMuscles do
  use Ecto.Migration

  def change do
    create table(:workouts_muscles) do
      add :workout_id, references(:workouts, on_delete: :delete_all)
      add :muscle_id, references(:muscles, on_delete: :delete_all)
    end

    create unique_index(:workouts_muscles, [:workout_id, :muscle_id])
  end
end
