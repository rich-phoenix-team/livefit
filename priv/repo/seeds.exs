# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     LiveFit.Repo.insert!(%LiveFit.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias LiveFit.Repo
alias LiveFit.Accounts.User
alias LiveFit.Fitness.{Workout, Category, Muscle}
alias LiveFit.Fitness

Repo.delete_all Category
Repo.delete_all Muscle
Repo.delete_all Workout
Repo.delete_all User

Repo.insert! %User{
  email: "admin@livefit.com",
  hashed_password: Bcrypt.hash_pwd_salt("000000000000"),
  role: :admin
}
Repo.insert! %User{
  email: "trainee@livefit.com",
  hashed_password: Bcrypt.hash_pwd_salt("000000000000"),
  role: :trainee
}
trainer1 = Repo.insert! %User{
  email: "trainer@livefit.com",
  hashed_password: Bcrypt.hash_pwd_salt("000000000000"),
  role: :trainer
}
trainer2 = Repo.insert! %User{
  email: "trainer2@livefit.com",
  hashed_password: Bcrypt.hash_pwd_salt("000000000000"),
  role: :trainer
}

Enum.each(3..10, fn(i) ->
  Repo.insert! %User{
    email: "trainer#{i}@livefit.com",
    hashed_password: Bcrypt.hash_pwd_salt("000000000000"),
    role: :trainer
  }
end)

for muscle <- ~w(arms legs chest back ass core) do
  Repo.insert! %Muscle{name: muscle}
end
muscles = Fitness.list_muscles()

for category <- ~w(Flexibility Strength Cardio) do
  Repo.insert! %Category{title: category}
end
categories = Fitness.list_categories()

for i <- 1..6 do
  Repo.insert! %Workout{
    title: "workout #{i}",
    link: "youtube",
    description: "super vid #{i}",
    user_id: trainer1.id,
    equipment_needed: Enum.random([true, false]),
    category_id: Enum.random([%{id: nil} | categories]).id,
    muscles: Enum.take_random(muscles, Enum.random(0..length(muscles)))
  }
end

Repo.insert! %Workout{
  title: "general workout",
  link: "facebook",
  description: "toit",
  user_id: trainer2.id,
  equipment_needed: Enum.random([true, false]),
  category_id: Enum.random([%{id: nil} | categories]).id,
  muscles: Enum.take_random(muscles, Enum.random(0..length(muscles)))
}
