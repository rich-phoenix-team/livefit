# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :livefit,
  namespace: LiveFit,
  ecto_repos: [LiveFit.Repo]

# Configures the endpoint
config :livefit, LiveFitWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "VFtTbAM9xtFYIcfhBCgGuvKL2yHqxdK3Ap45cp+dAzA1NuxYptesy15aqX0+F7w/",
  render_errors: [view: LiveFitWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: LiveFit.PubSub,
  live_view: [signing_salt: "Y2Zma5RI"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
