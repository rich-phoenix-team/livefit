// We need to import the CSS so that webpack will load it.
// The MiniCssExtractPlugin is used to separate it out into
// its own CSS file.
import "../css/app.scss"

// webpack automatically bundles all modules in your
// entry points. Those entry points can be configured
// in "webpack.config.js".
//
// Import deps with the dep name or local files with a relative path, for example:
//
//     import {Socket} from "phoenix"
//     import socket from "./socket"
//
import "phoenix_html"
import {Socket} from "phoenix"
import NProgress from "nprogress"
import {LiveSocket} from "phoenix_live_view"
import jQuery from "jquery"
import "select2"

window.jQuery = jQuery
window.$ = jQuery

let Hooks = {}
Hooks.Select2 = {
  mounted() {
    $(".select2-form-input").select2({theme: "bootstrap"})
  },
  updated() {
    $(".select2-form-input").select2({theme: "bootstrap"})
  }
}
Hooks.Select2Category = {
  initSelect2() {
    let hook = this
    
    $(hook.el).select2({theme: "bootstrap"})
      .on("select2:select", (e) => hook.selected(hook, e))
  },

  mounted() {
    this.initSelect2();
  },

  selected(hook, event) {
    let id = event.params.data.id
    hook.pushEvent("select2_selected", {category_id: id})
  }
}

Hooks.Select2Muscles = {
  initSelect2() {
    let hook = this
    
    $(hook.el).select2({theme: "bootstrap"})
      .on("change.select2", (e) => hook.selected(hook, e))
  },

  mounted() {
    this.initSelect2();
  },

  selected(hook, event) {
    let ids = []
    $(event.target).select2('data').forEach(function(el){ids.push(el.id)})
    hook.pushEvent("select2_selected", {muscle_ids: ids})
  }
}


let csrfToken = document.querySelector("meta[name='csrf-token']").getAttribute("content")
let liveSocket = new LiveSocket("/live", Socket, {params: {_csrf_token: csrfToken}, hooks: Hooks})

// Show progress bar on live navigation and form submits
window.addEventListener("phx:page-loading-start", info => NProgress.start())
window.addEventListener("phx:page-loading-stop", info => NProgress.done())

// connect if there are any LiveViews on the page
liveSocket.connect()

// expose liveSocket on window for web console debug logs and latency simulation:
// >> liveSocket.enableDebug()
// >> liveSocket.enableLatencySim(1000)  // enabled for duration of browser session
// >> liveSocket.disableLatencySim()
window.liveSocket = liveSocket


