defmodule LiveFitWeb.InteractionPolicy do

  @moduledoc """
  Defines interactions related functions.
  """

  alias LiveFit.Accounts.User
  alias LiveFit.Fitness.Workout

  @type entity :: struct()
  @type action :: :follow | :favorite | :view | :like

  @spec can?(%User{}, entity(), action()) :: boolean()
  def can?(user, entity, action)

  ### VIEWS ###
  def can?(%User{role: :trainee}, %Workout{}, :view), do: true

  ### FOLLOWS ###
  def can?(%User{role: :trainee}, %User{role: :trainer}, :follow), do: true

  ### FAVORITES ###
  def can?(%User{role: :trainee}, %Workout{}, :favorite), do: true

  ### FAVORITES ###
  def can?(%User{role: :trainee}, %Workout{}, :like), do: true

  def can?(_, _, _), do: false
end
