defmodule LiveFitWeb.RolePolicy do

  @moduledoc """
  Defines roles related functions.
  """

  alias LiveFit.Accounts.User
  alias LiveFit.Fitness.{Workout, Category, Muscle}
  alias LiveFit.Interactions.Comment

  @type entity :: struct()
  @type action :: :new | :index | :edit | :show | :delete

  @spec can?(%User{}, entity(), action()) :: boolean()
  def can?(user, entity, action)

  def can?(%User{role: :admin}, %Workout{}, _any), do: true
  def can?(%User{role: :admin}, %Category{}, _any), do: true
  def can?(%User{role: :admin}, %Muscle{}, _any), do: true
  def can?(%User{role: :admin}, %User{role: :trainer}, _any), do: true
  def can?(%User{role: :admin}, %Comment{}, _any), do: true

  def can?(%User{role: :trainer}, %Workout{}, :new), do: true
  def can?(%User{role: :trainer, id: id}, %Workout{user_id: id}, :edit), do: true
  def can?(%User{role: :trainer, id: id}, %Workout{user_id: id}, :delete), do: true

  def can?(%User{role: :trainee}, %User{role: :trainer}, :index), do: true
  def can?(%User{role: :trainee}, %User{role: :trainer}, :show), do: true

  def can?(%User{}, %Workout{}, :index), do: true
  def can?(%User{}, %Workout{}, :show), do: true
  def can?(%User{}, %Comment{}, :index), do: true
  def can?(%User{}, %Comment{}, :show), do: true
  def can?(%User{}, %Comment{}, :new), do: true
  def can?(%User{id: id}, %Comment{user_id: id}, :edit), do: true
  def can?(%User{id: id}, %Comment{user_id: id}, :delete), do: true

  def can?(_, _, _), do: false
end
