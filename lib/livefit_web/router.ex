defmodule LiveFitWeb.Router do
  use LiveFitWeb, :router

  import LiveFitWeb.UserAuth
  alias LiveFitWeb.AuthorizeRolePlug

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {LiveFitWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :fetch_current_user
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :trainer do
    plug AuthorizeRolePlug, [:trainer, :admin]
  end

  pipeline :trainee do
    plug AuthorizeRolePlug, [:trainee, :admin]
  end

  pipeline :admin do
    plug AuthorizeRolePlug, :admin
  end

  scope "/", LiveFitWeb do
    pipe_through :browser
    get "/users/force_logout", UserSessionController, :force_logout
    live "/", PageLive, :index
  end

  scope "/", LiveFitWeb do
    pipe_through [:browser, :require_authenticated_user, :admin]

    live "/categories", CategoryLive.Index, :index
    live "/categories/new", CategoryLive.Index, :new
    live "/categories/:id/edit", CategoryLive.Index, :edit

    live "/categories/:id", CategoryLive.Show, :show
    live "/categories/:id/show/edit", CategoryLive.Show, :edit

    live "/muscles", MuscleLive.Index, :index
    live "/muscles/new", MuscleLive.Index, :new
    live "/muscles/:id/edit", MuscleLive.Index, :edit

    live "/muscles/:id", MuscleLive.Show, :show
    live "/muscles/:id/show/edit", MuscleLive.Show, :edit

  end

  scope "/", LiveFitWeb do
    pipe_through [:browser, :require_authenticated_user, :trainer]

    live "/workouts/new", WorkoutLive.Index, :new
    live "/workouts/:id/edit", WorkoutLive.Index, :edit

    live "/workouts/:id/show/edit", WorkoutLive.Show, :edit
  end

  scope "/", LiveFitWeb do
    pipe_through [:browser, :require_authenticated_user, :trainee]

    live "/trainers", TrainerLive.Index, :index
    live "/trainers/:id", TrainerLive.Show, :show
  end

  # Other scopes may use custom stacks.
  # scope "/api", LiveFitWeb do
  #   pipe_through :api
  # end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser
      live_dashboard "/dashboard", metrics: LiveFitWeb.Telemetry
    end
  end

  ## Authentication routes

  scope "/", LiveFitWeb do
    pipe_through [:browser, :redirect_if_user_is_authenticated]

    get "/users/register", UserRegistrationController, :new
    post "/users/register", UserRegistrationController, :create
    get "/users/log_in", UserSessionController, :new
    post "/users/log_in", UserSessionController, :create
    get "/users/reset_password", UserResetPasswordController, :new
    post "/users/reset_password", UserResetPasswordController, :create
    get "/users/reset_password/:token", UserResetPasswordController, :edit
    put "/users/reset_password/:token", UserResetPasswordController, :update
  end

  scope "/", LiveFitWeb do
    pipe_through [:browser, :require_authenticated_user]

    # live "/comments", CommentLive.Index, :index
    # live "/comments/new", CommentLive.Index, :new
    # live "/comments/:id/edit", CommentLive.Index, :edit

    # live "/comments/:id", CommentLive.Show, :show
    # live "/comments/:id/show/edit", CommentLive.Show, :edit

    live "/workouts", WorkoutLive.Index, :index
    live "/workouts/:id", WorkoutLive.Show, :show

    get "/users/settings", UserSettingsController, :edit
    put "/users/settings/update_password", UserSettingsController, :update_password
    put "/users/settings/update_email", UserSettingsController, :update_email
    get "/users/settings/confirm_email/:token", UserSettingsController, :confirm_email
  end

  scope "/", LiveFitWeb do
    pipe_through [:browser]

    delete "/users/log_out", UserSessionController, :delete
    get "/users/confirm", UserConfirmationController, :new
    post "/users/confirm", UserConfirmationController, :create
    get "/users/confirm/:token", UserConfirmationController, :confirm
  end
end
