defmodule LiveFitWeb.CommentLive.FormComponent do
  use LiveFitWeb, :live_component

  alias LiveFit.Interactions

  @impl true
  def mount(socket) do
    {:ok, socket, temporary_assigns: [changeset: nil]}
  end

  @impl true
  def update(%{comment: comment} = assigns, socket) do
    changeset = Interactions.change_comment(comment)
    {:ok,
     socket
     |> assign(assigns)
     |> assign(:changeset, changeset)
    }
  end

  @impl true
  def handle_event("save", %{"comment" => comment_params}, socket) do
    action = socket.assigns.action
    comment = socket.assigns.comment
    current_user = socket.assigns.current_user

    if RolePolicy.can?(current_user, comment, action) do
      save_comment(socket, action, comment_params)
    else
      {:noreply,
       socket
       |> put_flash(:error, "Unauthorised")
       |> redirect(to: "/")}
    end
  end

  defp save_comment(socket, :edit, comment_params) do
    case Interactions.update_comment(socket.assigns.comment, comment_params) do
      {:ok, _edited_comment} ->
        {:noreply, socket}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, :changeset, changeset)}
    end
  end

  defp save_comment(socket, :reply, comment_params), do: save_comment(socket, :new, comment_params)
  defp save_comment(socket, :new, comment_params) do
    comment_params
    |> Map.put("workout_id", socket.assigns.workout_id)
    |> Map.put("parent_id", socket.assigns.comment.parent_id)
    |> Interactions.create_comment(socket.assigns.current_user)
    |> case do
      {:ok, _new_comment} ->
        {:noreply,
          socket
          |> assign(:changeset, Interactions.change_comment())
        }

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, changeset: changeset)}
    end
  end
end
