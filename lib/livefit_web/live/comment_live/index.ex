defmodule LiveFitWeb.CommentLive.Index do
  use LiveFitWeb, :live_view

  alias LiveFit.Interactions
  alias LiveFit.Interactions.Comment
  alias LiveFitWeb.RolePolicy

  @impl true
  def mount(_params, %{"workout_id" => workout_id} = session, socket) do
    if connected?(socket), do: Interactions.subscribe()
    socket = assign_defaults(session, socket)
    {:ok,
      socket
      |> assign(:comments, Interactions.list_root_comments(workout_id))
      |> assign(:workout_id, workout_id)
      |> assign(:comment, %Comment{}),
    temporary_assigns: [comments: []]}
  end

  @impl true
  def handle_info({:comment_created, comment}, socket) do
    if comment.parent_id do
      send_update(LiveFitWeb.CommentLive.CommentComponent, id: comment.parent_id, children: [comment], comment: Interactions.get_comment!(comment.parent_id), reply_form_visible: false, edit_form_visible: false, prevent_preload: true)
      {:noreply, socket}
    else
      {:noreply, assign(socket, comments: [comment])}
    end
  end

  @impl true
  def handle_info({:comment_updated, comment}, socket) do
    send_update(LiveFitWeb.CommentLive.CommentComponent, id: comment.id, comment: comment, reply_form_visible: false, edit_form_visible: false)
    {:noreply, socket}
  end

end
