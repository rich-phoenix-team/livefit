defmodule LiveFitWeb.CommentLive.CommentComponent do
  use LiveFitWeb, :live_component

  alias LiveFit.Interactions
  alias LiveFit.Interactions.Comment
  alias LiveFitWeb.RolePolicy

  def render(assigns) do
    ~L"""
    <article class="comment comment--<%= @kind %>" id="comment-<%= @id %>">
      <div class="comment-body">
        <%= if @edit_form_visible do %>
          <%= live_component @socket, LiveFitWeb.CommentLive.FormComponent,
            id: @id,
            action: :edit,
            current_user: @current_user,
            comment: @comment,
            workout_id: @workout_id %>
        <% else %>
          <%= if @comment.deleted_at do %>
            This comment was deleted
          <% else %>
            <%= @comment.body %>
          <% end %>
        <% end %>
      </div>

      <div class="comment-footer">
        <%= if !@edit_form_visible && !@comment.deleted_at do %>
          <span class="comment-reply">
            <a href="javascript:;" phx-click="toggle-reply" phx-target="#comment-<%= @id %>" title="Reply to this comment">reply</a>
          </span>
        <% end %>
        <%= if RolePolicy.can?(@current_user, @comment, :edit) && !@comment.deleted_at do %>
          <%= if !@reply_form_visible do %>
            <span class="comment-edit">
              <a href="javascript:;" phx-click="toggle-edit" phx-target="#comment-<%= @id %>" title="Edit this comment">edit</a>
            </span>
          <% end %>
        <% end %>
        <%= if RolePolicy.can?(@current_user, @comment, :delete) && !@comment.deleted_at do %>
          <%= if !@edit_form_visible && !@reply_form_visible do %>
            <span class="comment-delete">
              <a href="javascript:;" phx-click="delete" phx-target="#comment-<%= @id %>" title="Delete this comment" data-confirm="Are you sure?">delete</a>
            </span>
          <% end %>
        <% end %>

      </div>

      <%= if @reply_form_visible do %>
        <%= live_component @socket, LiveFitWeb.CommentLive.FormComponent,
          id: @id,
          action: :new,
          current_user: @current_user,
          comment: @form_comment,
          workout_id: @workout_id %>
      <% end %>

      <section class="comment-replies" phx-update="append" id="replies-<%= @id %>">
        <%= for child <- @children do %>
          <%= live_component @socket, LiveFitWeb.CommentLive.CommentComponent, id: child.id, comment: child, current_user: @current_user, workout_id: @workout_id, kind: :child %>
        <% end %>
      </section>
    </article>
    """

  end

  def mount(socket) do
    {:ok,
      socket
      |> assign(:reply_form_visible, false)
      |> assign(:edit_form_visible, false),
     temporary_assigns: [comment: nil, form_comment: nil, children: []]}
  end

  def preload(list_of_assigns) do
    {send_update_assigns, list_of_assigns} = Enum.split_with(list_of_assigns, fn a -> Map.get(a, :prevent_preload) end)
    if send_update_assigns == [] do
      parent_ids = Enum.map(list_of_assigns, & &1.id)
      children = Interactions.get_child_comments(parent_ids)

      Enum.map(list_of_assigns, fn assigns ->
        Map.put(assigns, :children, Map.get(children, assigns.id, []))
      end)
    else
      send_update_assigns
    end
  end

  def handle_event("toggle-reply", _, socket) do
    {:noreply,
      socket
      |> update(:reply_form_visible, &(!&1))
      |> assign(:comment, Interactions.get_comment!(socket.assigns.id))
      |> assign(:form_comment, %Comment{parent_id: socket.assigns.id})
    }
  end

  def handle_event("toggle-edit", _, socket) do
    {:noreply,
      socket
      |> update(:edit_form_visible, &(!&1))
      |> assign(:comment, Interactions.get_comment!(socket.assigns.id))
    }
  end

  def handle_event("delete", _, socket) do
    current_user = socket.assigns.current_user
    comment = Interactions.get_comment!(socket.assigns.id)

    if RolePolicy.can?(current_user, comment, :delete) do
      {:ok, deleted_comment} = Interactions.delete_comment(comment)
      {:noreply,
        socket
        |> assign(:comment, deleted_comment)
      }
    else
      {:noreply,
       socket
       |> put_flash(:error, "Unauthorised")
       |> redirect(to: "/")}
    end
  end

end
