defmodule LiveFitWeb.WorkoutLive.Index do
  use LiveFitWeb, :live_view

  alias LiveFit.Fitness
  alias LiveFit.Fitness.Workout
  alias LiveFitWeb.RolePolicy
  alias LiveFit.Interactions
  alias LiveFitWeb.InteractionPolicy

  @impl true
  def mount(_params, session, socket) do
    socket = assign_defaults(session, socket)
    search_changeset = Fitness.search_changeset(%Workout{}, %{equipment_needed: true})

    {:ok,
      socket
      |> assign(:search_changeset, search_changeset)
      |> assign(:categories, Fitness.list_categories())
      |> assign(:muscles, Fitness.list_muscles())
      |> assign(:workouts, list_user_workouts(socket.assigns.current_user, search_changeset))
    }
  end

  @impl true
  def handle_params(params, _url, socket) do
    current_user = socket.assigns.current_user
    live_action = socket.assigns.live_action
    workout = workout_from_params(current_user, params)

    if RolePolicy.can?(current_user, workout, live_action) do
      {:noreply, apply_action(socket, live_action, params)}
    else
      {:noreply,
       socket
       |> put_flash(:error, "Unauthorised")
       |> redirect(to: "/")}
    end
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Workout")
    |> assign(:workout, Fitness.get_workout!(socket.assigns.current_user, id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Workout")
    |> assign(:workout, Fitness.new_workout())
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Workouts")
    |> assign(:workout, nil)
  end

  def handle_event("search", %{"search" => search_params}, socket) do
    current_user = socket.assigns.current_user
    if RolePolicy.can?(current_user, Fitness.new_workout(), :index) do
      %Workout{}
      |> Fitness.search_changeset(search_params)
      |> case do
        new_changeset = %{valid?: true, changes: _changes} ->
          {:noreply,
            socket
            |> assign(:workouts, list_user_workouts(current_user, new_changeset))
            |> assign(:search_changeset, new_changeset)
          }
        _ ->
          {:noreply, socket}
      end
    else
      {:noreply,
       socket
       |> put_flash(:error, "Unauthorised")
       |> redirect(to: "/")}
    end
  end

  def handle_event("select2_selected", search_params, socket) do
    search_changeset = socket.assigns.search_changeset
    search_changeset
    |> Fitness.search_changeset(search_params)
    |> case do
      new_changeset = %{valid?: true, changes: _changes} ->
        {:noreply,
          socket
          |> assign(:workouts, list_user_workouts(socket.assigns.current_user, new_changeset))
          |> assign(:search_changeset, new_changeset)
        }
      _ ->
        {:noreply, socket}
    end
  end

  @impl true
  def handle_event("favorite", %{"workout_id" => workout_id}, socket) do
    current_user = socket.assigns.current_user
    workout = Fitness.get_workout!(current_user, workout_id)
    if InteractionPolicy.can?(current_user, workout, :favorite) do
      Interactions.add_favorite_workout(current_user, workout.id)
    end

    {:noreply, socket |> assign(:workouts, list_user_workouts(current_user, socket.assigns.search_changeset))}
  end

  @impl true
  def handle_event("unfavorite", %{"workout_id" => workout_id}, socket) do
    current_user = socket.assigns.current_user
    workout = Fitness.get_workout!(current_user, workout_id)
    if InteractionPolicy.can?(current_user, workout, :favorite) do
      Interactions.remove_favorite_workout(current_user, workout.id)
    end

    {:noreply, socket |> assign(:workouts, list_user_workouts(current_user, socket.assigns.search_changeset))}
  end

  @impl true
  def handle_event("like", %{"workout_id" => workout_id}, socket) do
    current_user = socket.assigns.current_user
    workout = Fitness.get_workout!(current_user, workout_id)
    if InteractionPolicy.can?(current_user, workout, :like) do
      Interactions.like_workout(current_user, workout.id)
    end

    {:noreply, socket |> assign(:workouts, list_user_workouts(current_user, socket.assigns.search_changeset))}
  end

  @impl true
  def handle_event("unlike", %{"workout_id" => workout_id}, socket) do
    current_user = socket.assigns.current_user
    workout = Fitness.get_workout!(current_user, workout_id)
    if InteractionPolicy.can?(current_user, workout, :like) do
      Interactions.unlike_workout(current_user, workout.id)
    end

    {:noreply, socket |> assign(:workouts, list_user_workouts(current_user, socket.assigns.search_changeset))}
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    current_user = socket.assigns.current_user
    search_changeset = socket.assigns.search_changeset
    workout = Fitness.get_workout!(current_user, id)

    if RolePolicy.can?(current_user, workout, :delete) do
      {:ok, _} = Fitness.delete_workout(workout)
      {:noreply, assign(socket, :workouts, list_user_workouts(current_user, search_changeset))}
    else
      {:noreply,
       socket
       |> put_flash(:error, "Unauthorised")
       |> redirect(to: "/")}
    end
  end

  defp list_user_workouts(user, changeset) do
    Fitness.search_workouts(user, changeset.changes)
  end

  defp workout_from_params(current_user, %{"id" => id}), do: Fitness.get_workout!(current_user, id)
  defp workout_from_params(_current_user, _params), do: Fitness.new_workout()

end
