defmodule LiveFitWeb.WorkoutLive.Show do
  use LiveFitWeb, :live_view

  alias LiveFit.Fitness
  alias LiveFitWeb.RolePolicy
  alias LiveFitWeb.InteractionPolicy
  alias LiveFit.Interactions

  @impl true
  def mount(_params, session, socket) do
    socket = assign_defaults(session, socket)
    {:ok, socket}
  end

  @impl true
  def handle_params(%{"id" => id}, _, socket) do
    current_user = socket.assigns.current_user
    live_action = socket.assigns.live_action
    workout = Fitness.get_workout!(current_user, id)

    if RolePolicy.can?(current_user, workout, live_action) do
      if InteractionPolicy.can?(current_user, workout, :view) do
        Interactions.add_workout_view(current_user, workout.id)
      end

      view_count = Interactions.get_view_count(workout.id)
      like_count = Interactions.get_like_count(workout.id)

      {:noreply,
        socket
        |> assign(:page_title, page_title(live_action))
        |> assign(:workout, workout)
        |> assign(:like_count, like_count)
        |> assign(:view_count, view_count)}
    else
      {:noreply,
        socket
        |> put_flash(:error, "Unauthorised")
        |> redirect(to: "/")}
    end

  end

  @impl true
  def handle_event("favorite", _params, socket) do
    current_user = socket.assigns.current_user
    workout = socket.assigns.workout
    if InteractionPolicy.can?(current_user, workout, :favorite) do
      Interactions.add_favorite_workout(current_user, workout.id)
    end

    {:noreply, socket |> assign(:workout, Fitness.get_workout!(current_user, workout.id))}
  end

  @impl true
  def handle_event("unfavorite", _params, socket) do
    current_user = socket.assigns.current_user
    workout = socket.assigns.workout
    if InteractionPolicy.can?(current_user, workout, :favorite) do
      Interactions.remove_favorite_workout(current_user, workout.id)
    end

    {:noreply, socket |> assign(:workout, Fitness.get_workout!(current_user, workout.id))}
  end

  @impl true
  def handle_event("like", _params, socket) do
    current_user = socket.assigns.current_user
    workout = socket.assigns.workout
    if InteractionPolicy.can?(current_user, workout, :like) do
      Interactions.like_workout(current_user, workout.id)
    end

    {:noreply,
      socket
      |> assign(:like_count, Interactions.get_like_count(workout.id))
      |> assign(:workout, Fitness.get_workout!(current_user, workout.id))}
  end

  @impl true
  def handle_event("unlike", _params, socket) do
    current_user = socket.assigns.current_user
    workout = socket.assigns.workout
    if InteractionPolicy.can?(current_user, workout, :like) do
      Interactions.unlike_workout(current_user, workout.id)
    end

    {:noreply,
      socket
      |> assign(:like_count, Interactions.get_like_count(workout.id))
      |> assign(:workout, Fitness.get_workout!(current_user, workout.id))}
  end

  defp page_title(:show), do: "Show Workout"
  defp page_title(:edit), do: "Edit Workout"

  defp embed_youtube_link(link), do: String.replace(link, "/watch?v=", "/embed/")
  defp format_youtube_link("https://" <> _rest = link), do: embed_youtube_link(link)
  defp format_youtube_link("http://" <> _rest = link), do: embed_youtube_link(link)
  defp format_youtube_link(link), do: "https://" <> embed_youtube_link(link)
  defp is_youtube_link(link), do: String.contains?(link, ["youtube.com", "youtu.be"])
  defp is_facebook_link(link), do: String.contains?(link, ["facebook.com", "fb.com", "fb.watch"])
end
