defmodule LiveFitWeb.WorkoutLive.WorkoutComponent do
  use LiveFitWeb, :live_component

  alias LiveFitWeb.RolePolicy
  alias LiveFitWeb.InteractionPolicy

  def render(assigns) do
    ~L"""
    <tr id="workout-<%= @workout.id %>">

        <%= if InteractionPolicy.can?(@current_user, @workout, :favorite) do %>
          <td>
            <%= if @workout.is_favorite do %>
              <button phx-click="unfavorite" phx-value-workout_id="<%= @workout.id %>">unfavorite</button>
            <% else %>
              <button phx-click="favorite" phx-value-workout_id="<%= @workout.id %>">favorite</button>
            <% end %>
          </td>
        <% end %>
        <%= if InteractionPolicy.can?(@current_user, @workout, :like) do %>
          <td>
            <%= if @workout.is_liked do %>
              <button phx-click="unlike" phx-value-workout_id="<%= @workout.id %>">unlike</button>
            <% else %>
              <button phx-click="like" phx-value-workout_id="<%= @workout.id %>">like</button>
            <% end %>
          </td>
        <% end %>
        <%= if !trainer?(@current_user) do %>
          <td>
            <%= @workout.user.email %>
          </td>
        <% end %>
        <td>
          <%= @workout.title %>
        </td>
        <td>
          <%= @workout.link %>
        </td>
        <td>
          <%= @workout.description %>
        </td>
        <td><%= @workout.category %></td>
        <td><%= join_list(@workout.muscles) %></td>
        <td><%= @workout.equipment_needed %></td>
        <td><%= @workout.view_count %></td>
        <td><%= @workout.like_count %></td>
        <td>
          <%= if RolePolicy.can?(@current_user, @workout, :show) do %>
            <span><%= live_redirect "Show", to: Routes.workout_show_path(@socket, :show, @workout.id) %></span>
          <% end %>
          <%= if RolePolicy.can?(@current_user, @workout, :edit) do %>
            <span><%= live_patch "Edit", to: Routes.workout_index_path(@socket, :edit, @workout.id) %></span>
          <% end %>
          <%= if RolePolicy.can?(@current_user, @workout, :delete) do %>
            <span><%= link "Delete", to: "#", phx_click: "delete", phx_value_id: @workout.id, data: [confirm: "Are you sure?"] %></span>
          <% end %>
        </td>
      </tr>
    """

  end

end
