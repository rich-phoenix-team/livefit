defmodule LiveFitWeb.MuscleLive.Index do
  use LiveFitWeb, :live_view

  alias LiveFit.Fitness
  alias LiveFit.Fitness.Muscle
  alias LiveFitWeb.RolePolicy

  @impl true
  def mount(_params, session, socket) do
    socket = assign_defaults(session, socket)
    {:ok, assign(socket, :muscles, list_muscles())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    current_user = socket.assigns.current_user
    live_action = socket.assigns.live_action
    muscle = muscle_from_params(params)

    if RolePolicy.can?(current_user, muscle, live_action) do
      {:noreply, apply_action(socket, live_action, params)}
    else
      {:noreply,
       socket
       |> put_flash(:error, "Unauthorised")
       |> redirect(to: "/")}
    end
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Muscle")
    |> assign(:muscle, Fitness.get_muscle!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Muscle")
    |> assign(:muscle, %Muscle{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Muscles")
    |> assign(:muscle, nil)
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    current_user = socket.assigns.current_user
    muscle = Fitness.get_muscle!(id)

    if RolePolicy.can?(current_user, muscle, :delete) do
      {:ok, _} = Fitness.delete_muscle(muscle)
      {:noreply, assign(socket, :muscles, list_muscles())}
    else
      {:noreply,
       socket
       |> put_flash(:error, "Unauthorised")
       |> redirect(to: "/")}
    end
  end

  defp list_muscles do
    Fitness.list_muscles()
  end

  defp muscle_from_params(%{"id" => id}), do: Fitness.get_muscle!(id)
  defp muscle_from_params(_params), do: %Muscle{}
end
