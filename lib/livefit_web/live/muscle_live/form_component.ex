defmodule LiveFitWeb.MuscleLive.FormComponent do
  use LiveFitWeb, :live_component

  alias LiveFit.Fitness

  @impl true
  def update(%{muscle: muscle} = assigns, socket) do
    changeset = Fitness.change_muscle(muscle)

    {:ok,
     socket
     |> assign(assigns)
     |> assign(:changeset, changeset)}
  end

  @impl true
  def handle_event("validate", %{"muscle" => muscle_params}, socket) do
    changeset =
      socket.assigns.muscle
      |> Fitness.change_muscle(muscle_params)
      |> Map.put(:action, :validate)

    {:noreply, assign(socket, :changeset, changeset)}
  end

  def handle_event("save", %{"muscle" => muscle_params}, socket) do
    save_muscle(socket, socket.assigns.action, muscle_params)
  end

  defp save_muscle(socket, :edit, muscle_params) do
    case Fitness.update_muscle(socket.assigns.muscle, muscle_params) do
      {:ok, _muscle} ->
        {:noreply,
         socket
         |> put_flash(:info, "Muscle updated successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, :changeset, changeset)}
    end
  end

  defp save_muscle(socket, :new, muscle_params) do
    case Fitness.create_muscle(muscle_params) do
      {:ok, _muscle} ->
        {:noreply,
         socket
         |> put_flash(:info, "Muscle created successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, changeset: changeset)}
    end
  end
end
