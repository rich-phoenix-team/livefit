defmodule LiveFitWeb.LiveHelpers do
  import Phoenix.LiveView.Helpers
  import Phoenix.LiveView

  alias LiveFit.Accounts
  alias LiveFit.Accounts.User
  alias LiveFitWeb.Router.Helpers, as: Routes
  alias LiveFitWeb.UserAuth

  @doc """
  Renders a component inside the `LiveFitWeb.ModalComponent` component.

  The rendered modal receives a `:return_to` option to properly update
  the URL when the modal is closed.

  ## Examples

      <%= live_modal @socket, LiveFitWeb.WorkoutLive.FormComponent,
        id: @workout.id || :new,
        action: @live_action,
        workout: @workout,
        return_to: Routes.workout_index_path(@socket, :index) %>
  """
  def live_modal(socket, component, opts) do
    path = Keyword.fetch!(opts, :return_to)
    modal_opts = [id: :modal, return_to: path, component: component, opts: opts]
    live_component(socket, LiveFitWeb.ModalComponent, modal_opts)
  end

  def assign_defaults(session, socket) do
    LiveFitWeb.Endpoint.subscribe(UserAuth.pubsub_topic())
    socket =
      assign_new(socket, :current_user, fn ->
        find_current_user(session)
      end)

    case socket.assigns.current_user do
      %User{} ->
        socket

      _other ->
        socket
        |> put_flash(:error, "You must log in to access this page.")
        |> redirect(to: Routes.user_session_path(socket, :new))
    end
  end

  def join_list(muscles, joiner \\ ", ") do
    Enum.map_join(muscles, joiner, &(&1.name))
  end

  defp find_current_user(session) do
    with user_token when not is_nil(user_token) <- session["user_token"],
         %User{} = user <- Accounts.get_user_by_session_token(user_token),
         do: user
  end

  def trainer?(user), do: User.trainer?(user)
  def trainee?(user), do: User.trainee?(user)
  def admin?(user), do: User.admin?(user)
end
