defmodule LiveFitWeb.TrainerLive.TrainerComponent do
  use LiveFitWeb, :live_component

  def render(assigns) do
    ~L"""
    <tr id="trainer-<%= @trainer.id %>">

        <%= if InteractionPolicy.can?(@current_user, @trainer, :follow) do %>
          <td>
            <%= if @trainer.is_followed do %>
              <button phx-click="unfollow" phx-value-trainer_id="<%= @trainer.id %>">unfollow</button>
            <% else %>
              <button phx-click="follow" phx-value-trainer_id="<%= @trainer.id %>">follow</button>
            <% end %>
          </td>
        <% end %>
        <td>
          <%= @trainer.email %>
        </td>
        <td>
          <%= @trainer.workout_count %>
        </td>
        <td>
          <span><%= live_redirect "Show", to: Routes.trainer_show_path(@socket, :show, @trainer.id) %></span>
        </td>
      </tr>
    """

  end
end
