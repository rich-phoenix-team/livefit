defmodule LiveFitWeb.TrainerLive.Show do
  use LiveFitWeb, :live_view

  alias LiveFit.Interactions
  alias LiveFitWeb.RolePolicy
  alias LiveFitWeb.InteractionPolicy

  @impl true
  def mount(_params, session, socket) do
    socket = assign_defaults(session, socket)
    {:ok, socket}
  end

  @impl true
  def handle_params(%{"id" => id}, _, socket) do
    current_user = socket.assigns.current_user
    live_action = socket.assigns.live_action
    trainer = Interactions.get_trainer!(current_user, id)

    if RolePolicy.can?(current_user, trainer, live_action) do
      {:noreply,
        socket
        |> assign(:page_title, "Show trainer")
        |> assign(:trainer, trainer)}
    else
      {:noreply,
        socket
        |> put_flash(:error, "Unauthorised")
        |> redirect(to: "/")}
    end
  end

  @impl true
  def handle_event("follow", _params, socket) do
    current_user = socket.assigns.current_user
    trainer = socket.assigns.trainer
    if InteractionPolicy.can?(current_user, trainer, :follow) do
      Interactions.follow(current_user, trainer.id)
    end

    {:noreply, socket |> assign(:trainer, Interactions.get_trainer!(current_user, trainer.id))}
  end

  @impl true
  def handle_event("unfollow", _params, socket) do
    current_user = socket.assigns.current_user
    trainer = socket.assigns.trainer
    if InteractionPolicy.can?(current_user, trainer, :follow) do
      Interactions.unfollow(current_user, trainer.id)
    end

    {:noreply, socket |> assign(:trainer, Interactions.get_trainer!(current_user, trainer.id))}
  end

end
