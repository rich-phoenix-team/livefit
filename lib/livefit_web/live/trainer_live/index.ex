defmodule LiveFitWeb.TrainerLive.Index do
  use LiveFitWeb, :live_view

  alias LiveFit.Interactions
  alias LiveFitWeb.RolePolicy
  alias LiveFitWeb.InteractionPolicy

  @impl true
  def mount(_params, session, socket) do
    socket = assign_defaults(session, socket)

    {:ok,
      assign(socket, :trainers, list_trainers(socket.assigns.current_user))
    }
  end

  @impl true
  def handle_params(params, _url, socket) do
    current_user = socket.assigns.current_user
    live_action = socket.assigns.live_action
    trainer = trainer_from_params(current_user, params)

    if RolePolicy.can?(current_user, trainer, live_action) do
      {:noreply, apply_action(socket, live_action, params)}
    else
      {:noreply,
       socket
       |> put_flash(:error, "Unauthorised")
       |> redirect(to: "/")}
    end
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Trainers")
    |> assign(:trainer, nil)
  end

  @impl true
  def handle_event("follow", %{"trainer_id" => trainer_id}, socket) do
    current_user = socket.assigns.current_user
    trainer = LiveFit.Accounts.get_user!(trainer_id)
    if InteractionPolicy.can?(current_user, trainer, :follow) do
      Interactions.follow(current_user, trainer.id)
    end

    {:noreply, socket |> assign(:trainers, Interactions.list_trainers(current_user))}
  end

  @impl true
  def handle_event("unfollow", %{"trainer_id" => trainer_id}, socket) do
    current_user = socket.assigns.current_user
    trainer = LiveFit.Accounts.get_user!(trainer_id)
    if InteractionPolicy.can?(current_user, trainer, :follow) do
      Interactions.unfollow(current_user, trainer.id)
    end

    {:noreply, socket |> assign(:trainers, Interactions.list_trainers(current_user))}
  end

  defp list_trainers(current_user) do
    Interactions.list_trainers(current_user)
  end

  defp trainer_from_params(current_user, %{"id" => id}), do: Interactions.get_trainer!(current_user, id)
  defp trainer_from_params(_current_user, _params), do: %User{role: :trainer}
end
