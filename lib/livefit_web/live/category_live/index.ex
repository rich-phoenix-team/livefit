defmodule LiveFitWeb.CategoryLive.Index do
  use LiveFitWeb, :live_view

  alias LiveFit.Fitness
  alias LiveFit.Fitness.Category
  alias LiveFitWeb.RolePolicy

  @impl true
  def mount(_params, session, socket) do
    socket = assign_defaults(session, socket)
    {:ok, assign(socket, :categories, list_categories())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    current_user = socket.assigns.current_user
    live_action = socket.assigns.live_action
    category = category_from_params(params)

    if RolePolicy.can?(current_user, category, live_action) do
      {:noreply, apply_action(socket, live_action, params)}
    else
      {:noreply,
       socket
       |> put_flash(:error, "Unauthorised")
       |> redirect(to: "/")}
    end
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Category")
    |> assign(:category, Fitness.get_category!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Category")
    |> assign(:category, %Category{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Categories")
    |> assign(:category, nil)
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    current_user = socket.assigns.current_user
    category = Fitness.get_category!(id)

    if RolePolicy.can?(current_user, category, :delete) do
      {:ok, _} = Fitness.delete_category(category)
      {:noreply, assign(socket, :categories, list_categories())}
    else
      {:noreply,
       socket
       |> put_flash(:error, "Unauthorised")
       |> redirect(to: "/")}
    end
  end

  defp list_categories do
    Fitness.list_categories()
  end

  defp category_from_params(%{"id" => id}), do: Fitness.get_category!(id)
  defp category_from_params(_params), do: %Category{}
end
