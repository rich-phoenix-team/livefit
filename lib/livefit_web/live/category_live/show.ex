defmodule LiveFitWeb.CategoryLive.Show do
  use LiveFitWeb, :live_view

  alias LiveFit.Fitness
  alias LiveFitWeb.RolePolicy

  @impl true
  def mount(_params, session, socket) do
    socket = assign_defaults(session, socket)
    {:ok, socket}
  end

  @impl true
  def handle_params(%{"id" => id}, _, socket) do
    current_user = socket.assigns.current_user
    live_action = socket.assigns.live_action
    category = Fitness.get_category!(id)

    if RolePolicy.can?(current_user, category, live_action) do
      {:noreply,
        socket
        |> assign(:page_title, page_title(live_action))
        |> assign(:category, category)}
    else
      {:noreply,
        socket
        |> put_flash(:error, "Unauthorised")
        |> redirect(to: "/")}
    end
  end

  defp page_title(:show), do: "Show Category"
  defp page_title(:edit), do: "Edit Category"
end
