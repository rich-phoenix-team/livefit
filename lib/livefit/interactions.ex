defmodule LiveFit.Interactions do
  @moduledoc """
  The Interactions context.
  """

  import Ecto.Query, warn: false
  alias LiveFit.Repo
  alias Ecto.Changeset

  alias LiveFit.Accounts.User
  alias LiveFit.Interactions.{UserFollowing, FavoriteWorkout, WorkoutView, WorkoutLike}
  alias LiveFit.Fitness.Workout


  @doc """
  Returns the list of trainers.

  ## Examples

      iex> list_trainers()
      [%User{role: :trainer}, ...]

  """
  def list_trainers(user) do
    User
    |> trainer_query(user.id)
    |> Repo.all
  end

  @doc """
  Gets a single trainer.

  Raises if the Trainer does not exist.

  ## Examples

      iex> get_trainer!(123)
      %User{role: :trainer}

  """
  def get_trainer!(user, id) do
    User
    |> trainer_query(user.id)
    |> Repo.get!(id)
  end

  ### FOLLOWING ###

  def list_followed_trainers(%User{role: :trainee} = user) do
    user
    |> Ecto.assoc(:followed)
    |> simple_trainer_query
    |> Repo.all
  end

  def unfollow(%User{id: trainee_id}, trainer_id) do
    UserFollowing
    |> where([uf], uf.follower_id == ^trainee_id and uf.followed_id == ^trainer_id)
    |> Repo.delete_all
  end

  def follow(%User{id: trainee_id}, trainer_id) do
    %UserFollowing{follower_id: trainee_id, followed_id: trainer_id}
    |> Repo.insert(on_conflict: :nothing)
  end


  ### FAVORITE ###

  def list_favorite_workouts(%User{} = user) do
    user
    |> Ecto.assoc(:favorite_workouts)
    |> Repo.all
    |> Repo.preload([:muscles, :category])
  end

  def add_favorite_workout(%User{id: user_id}, workout_id) do
    %FavoriteWorkout{user_id: user_id, workout_id: workout_id}
    |> Repo.insert(on_conflict: :nothing)
  end

  def remove_favorite_workout(%User{id: user_id}, workout_id) do
    FavoriteWorkout
    |> where([fw], fw.user_id == ^user_id and fw.workout_id == ^workout_id)
    |> Repo.delete_all
  end

  ### VIEWS ###

  def list_viewed_workouts(user) do
    user
    |> Ecto.assoc(:workout_views)
    |> Repo.all
    |> Repo.preload([:muscles, :category])
  end

  def get_view_count(workout_id) do
    WorkoutView
    |> where([wv], wv.workout_id == ^workout_id)
    |> Repo.aggregate(:count)
  end

  def add_workout_view(%User{} = user, %Workout{id: workout_id}), do: add_workout_view(user, workout_id)
  def add_workout_view(%User{id: user_id}, workout_id) do
    %WorkoutView{user_id: user_id, workout_id: workout_id}
    |> Repo.insert(on_conflict: :nothing)
  end

  ### LIKES ###

  def list_liked_workouts(user) do
    user
    |> Ecto.assoc(:workout_likes)
    |> Repo.all
    |> Repo.preload([:muscles, :category])
  end

  def unlike_workout(%User{id: trainee_id}, workout_id) do
    WorkoutLike
    |> where([wl], wl.user_id == ^trainee_id and wl.workout_id == ^workout_id)
    |> Repo.delete_all
  end

  def like_workout(%User{id: trainee_id}, workout_id) do
    %WorkoutLike{user_id: trainee_id, workout_id: workout_id}
    |> Repo.insert(on_conflict: :nothing)
  end

  def get_like_count(workout_id) do
    WorkoutLike
    |> where([wl], wl.workout_id == ^workout_id)
    |> Repo.aggregate(:count)
  end

  ### QUERIES ###

  def trainer_query(query, user_id) do
    query
    |> where([u], u.role == :trainer)
    |> join(:left, [u], f in UserFollowing, on: f.follower_id == ^user_id and f.followed_id == u.id)
    |> select_merge([u, ..., f], %{is_followed: max(f.follower_id) == ^user_id})
    |> simple_trainer_query
  end

  def simple_trainer_query(query) do
    from q in query,
    left_join: w in assoc(q, :workouts),
    group_by: q.id,
    select_merge: %{workout_count: count(w.id)}
  end

  alias LiveFit.Interactions.Comment

  @doc """
  Returns the list of comments.

  ## Examples

      iex> list_comments()
      [%Comment{}, ...]

  """
  def list_comments do
    Repo.all(Comment)
  end

  @doc """
  Returns the list of root comments for specific workout.
  ## Examples
      iex> list_root_comments(workout_id)
      [%Comment{}, ...]
  """
  def list_root_comments(workout_id) do
    Comment
    |> where([c], is_nil(c.parent_id))
    |> where([c], c.workout_id == ^workout_id)
    |> order_by([c], asc: :inserted_at)
    |> Repo.all()
  end

  @doc """
  Gets all child comments for a list of parent comment IDs.
  Returns results as a map grouped by parent_id.
  """
  def get_child_comments(parent_ids) do
    from(c in Comment, where: c.parent_id in ^parent_ids)
    |> order_by([c], asc: :inserted_at)
    |> Repo.all()
    |> Enum.group_by(&(&1.parent_id))
  end

  @doc """
  Gets a single comment.

  Raises `Ecto.NoResultsError` if the Comment does not exist.

  ## Examples

      iex> get_comment!(123)
      %Comment{}

      iex> get_comment!(456)
      ** (Ecto.NoResultsError)

  """
  def get_comment!(id), do: Repo.get!(Comment, id)

  @doc """
  Creates a comment.

  ## Examples

      iex> create_comment(%{field: value}, %User{})
      {:ok, %Comment{}}

      iex> create_comment(%{field: bad_value}, %User{})
      {:error, %Ecto.Changeset{}}

  """
  def create_comment(attrs, %User{} = user) do
    %Comment{}
    |> Comment.changeset(attrs)
    |> Changeset.put_assoc(:user, user)
    |> Repo.insert()
    |> broadcast(:comment_created)
  end

  @doc """
  Updates a comment.

  ## Examples

      iex> update_comment(comment, %{field: new_value})
      {:ok, %Comment{}}

      iex> update_comment(comment, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_comment(%Comment{} = comment, attrs) do
    comment
    |> Comment.changeset(attrs)
    |> Repo.update()
    |> broadcast(:comment_updated)
  end

  @doc """
  Deletes a comment.

  ## Examples

      iex> delete_comment(comment)
      {:ok, %Comment{}}

      iex> delete_comment(comment)
      {:error, %Ecto.Changeset{}}

  """
  def delete_comment(%Comment{} = comment) do
    comment
    |> Comment.delete_changeset(%{deleted_at: DateTime.utc_now()})
    |> Repo.update()
    |> broadcast(:comment_updated)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking comment changes.

  ## Examples

      iex> change_comment(comment)
      %Ecto.Changeset{data: %Comment{}}

  """
  def change_comment(), do: change_comment(%Comment{})
  def change_comment(%Comment{} = comment, attrs \\ %{}) do
    Comment.changeset(comment, attrs)
  end

  def subscribe do
    Phoenix.PubSub.subscribe(LiveFit.PubSub, "comments")
  end

  defp broadcast({:error, _reason} = error, _event), do: error

  defp broadcast({:ok, comment}, event) do
    Phoenix.PubSub.broadcast(LiveFit.PubSub, "comments", {event, comment})
    {:ok, comment}
  end
end
