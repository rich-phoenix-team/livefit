defmodule LiveFit.Fitness.Category do
  use Ecto.Schema
  import Ecto.Changeset

  schema "categories" do
    field :title, :string
    has_many :workouts, LiveFit.Fitness.Workout

    timestamps()
  end

  @doc false
  def changeset(category, attrs) do
    category
    |> cast(attrs, [:title])
    |> validate_required([:title])
    |> unique_constraint(:title)
  end
end

defimpl Phoenix.HTML.Safe, for: LiveFit.Fitness.Category do
  def to_iodata(category) do
    Phoenix.HTML.Safe.to_iodata(category.title)
  end
end
