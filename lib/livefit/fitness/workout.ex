defmodule LiveFit.Fitness.Workout do
  use Ecto.Schema
  import Ecto.Changeset

  schema "workouts" do
    field :description, :string
    field :link, :string
    field :title, :string
    field :equipment_needed, :boolean

    field :search_phrase, :string, virtual: true
    field :muscle_ids, {:array, :integer}, virtual: true
    field :is_favorite, :boolean, virtual: true
    field :is_liked, :boolean, virtual: true
    field :view_count, :integer, virtual: true
    field :like_count, :integer, virtual: true

    belongs_to :category, LiveFit.Fitness.Category
    belongs_to :user, LiveFit.Accounts.User
    has_many :comments, LiveFit.Interactions.Comment
    many_to_many :muscles, LiveFit.Fitness.Muscle, join_through: "workouts_muscles", on_replace: :delete

    # many_to_many :user_favorites,
    #   LiveFit.Accounts.User,
    #   join_through: LiveFit.Interactions.FavoriteWorkout

    many_to_many :user_views,
      LiveFit.Accounts.User,
      join_through: LiveFit.Interactions.WorkoutView

    timestamps()

  end

  def search_changeset(search, attrs \\ %{}) do
    search
    |> cast(attrs, [:search_phrase, :equipment_needed, :category_id, :muscle_ids])
    |> update_change(:search_phrase, &String.trim/1)
  end

  @doc false
  def changeset(workout, attrs) do
    workout
    |> cast(attrs, [:title, :link, :description, :category_id, :equipment_needed])
    |> validate_required([:title, :link])
    |> maybe_put_muscles_assoc(attrs)
  end

  defp maybe_put_muscles_assoc(changeset, %{muscles: muscle_ids}), do: maybe_put_muscles_assoc(changeset, %{"muscles" => muscle_ids})
  defp maybe_put_muscles_assoc(changeset, %{"muscles" => muscle_ids}) do
    muscles = LiveFit.Fitness.get_muscles_by_id(muscle_ids)
    put_assoc(changeset, :muscles, muscles)
  end
  defp maybe_put_muscles_assoc(changeset, %{}) do
    changeset
  end

end
