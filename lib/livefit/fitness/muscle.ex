defmodule LiveFit.Fitness.Muscle do
  use Ecto.Schema
  import Ecto.Changeset

  schema "muscles" do
    field :name, :string

    many_to_many :workouts, LiveFit.Fitness.Workout, join_through: "workouts_muscles"

    timestamps()
  end

  @doc false
  def changeset(muscle, attrs) do
    muscle
    |> cast(attrs, [:name])
    |> validate_required([:name])
    |> unique_constraint(:name)
  end
end
