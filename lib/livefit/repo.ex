defmodule LiveFit.Repo do
  use Ecto.Repo,
    otp_app: :livefit,
    adapter: Ecto.Adapters.Postgres
end
