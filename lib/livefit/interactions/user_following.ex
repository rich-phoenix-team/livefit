defmodule LiveFit.Interactions.UserFollowing do
  use Ecto.Schema

  schema "user_followings" do
    belongs_to :follower, LiveFit.Accounts.User
    belongs_to :followed, LiveFit.Accounts.User
  end

end
