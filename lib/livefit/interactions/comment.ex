defmodule LiveFit.Interactions.Comment do
  use Ecto.Schema
  import Ecto.Changeset

  schema "comments" do
    field :body, :string
    field :deleted_at, :utc_datetime


    belongs_to :user, LiveFit.Accounts.User
    belongs_to :workout, LiveFit.Fitness.Workout
    belongs_to :parent, __MODULE__, foreign_key: :parent_id
    has_many :children, __MODULE__, foreign_key: :parent_id

    timestamps()
  end

  @doc false
  def changeset(comment, attrs) do
    comment
    |> cast(attrs, [:body, :parent_id, :workout_id])
    |> validate_required([:body, :workout_id])
  end

  @doc false
  def delete_changeset(comment, attrs) do
    comment
    |> cast(attrs, [:deleted_at])
  end
end
