defmodule LiveFit.Interactions.WorkoutLike do
  use Ecto.Schema

  schema "workout_likes" do
    belongs_to :user, LiveFit.Accounts.User
    belongs_to :workout, LiveFit.Fitness.Workout
  end
end
