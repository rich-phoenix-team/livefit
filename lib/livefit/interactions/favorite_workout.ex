defmodule LiveFit.Interactions.FavoriteWorkout do
  use Ecto.Schema

  schema "favorite_workouts" do
    belongs_to :user, LiveFit.Accounts.User
    belongs_to :workout, LiveFit.Fitness.Workout
  end

end
