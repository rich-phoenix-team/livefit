defmodule LiveFit.Interactions.WorkoutView do
  use Ecto.Schema

  schema "workout_views" do
    belongs_to :user, LiveFit.Accounts.User
    belongs_to :workout, LiveFit.Fitness.Workout
  end
end
