defmodule LiveFit.Fitness do
  @moduledoc """
  The Fitness context.
  """

  import Ecto.Query, warn: false
  alias LiveFit.Repo
  alias Ecto.Changeset

  alias LiveFit.Fitness.Workout
  alias LiveFit.Accounts.User

  @doc """
  Returns the list of workouts.

  ## Examples

      iex> list_workouts()
      [%Workout{}, ...]

  """
  def list_workouts() do
    Workout
    |> workout_views_query()
    |> workout_likes_query()
    |> Repo.all
    |> Repo.preload([:category, :muscles])
  end

  @doc """
  Returns the list of workouts of specific user.

  ## Examples

      iex> list_user_workouts(user)
      [%Workout{}, ...]

  """
  def list_user_workouts(%User{} = user) do
    Workout
    |> user_workout_query(user)
    |> workout_is_favorite_query(user.id)
    |> workout_is_liked_query(user.id)
    |> workout_views_query()
    |> workout_likes_query()
    |> Repo.all
    |> Repo.preload([:category, :muscles])
  end

  def search_workouts(user = %User{}, attrs) do
    search_phrase = get_in(attrs, [:search_phrase])
    # true = all workouts. false = only with equipment_needed == false
    allow_equipment = get_in(attrs, [:equipment_needed])
    category_id = get_in(attrs, [:category_id])
    muscle_ids = get_in(attrs, [:muscle_ids])

    Workout
    |> distinct(true)
    |> search_phrase_query(search_phrase)
    |> workout_is_favorite_query(user.id)
    |> workout_is_liked_query(user.id)
    |> user_workout_query(user)
    |> workout_views_query()
    |> workout_likes_query()
    |> allow_equipment_query(allow_equipment)
    |> categories_query(category_id)
    |> muscles_query(muscle_ids)
    |> Repo.all
    |> Repo.preload([:category, :muscles, :user])
  end

  defp search_phrase_query(query, nil), do: query
  defp search_phrase_query(query, search_phrase) do
    s = "%#{search_phrase}%"
    query
    |> where([w], ilike(w.title, ^s))
    |> or_where([w], ilike(w.description, ^s))
  end
  def muscles_query(query, nil), do: query
  def muscles_query(query, ids) do
    query
    |> join(:left, [w], m in assoc(w, :muscles))
    |> group_by([w], w.id)
    |> having([w, ..., m], fragment("? <@ array_agg(?)", ^ids, m.id))
  end
  defp categories_query(query, nil), do: query
  defp categories_query(query, category_id), do: query |> where([w], w.category_id == ^category_id)
  defp allow_equipment_query(query, false), do: query |> where([w], w.equipment_needed == false)
  defp allow_equipment_query(query, _true), do: query

  @doc """
  Gets a single workout.

  Raises `Ecto.NoResultsError` if the Workout does not exist.

  ## Examples

      iex> get_workout!(123)
      %Workout{}

      iex> get_workout!(456)
      ** (Ecto.NoResultsError)

  """
  def get_workout!(%User{id: user_id}, id) do
    Workout
    |> workout_is_favorite_query(user_id)
    |> workout_is_liked_query(user_id)
    |> Repo.get!(id)
    |> Repo.preload([:category, :muscles, :user])
  end

  @doc """
  Returns an `Workout{}` with preloaded empty muscles.

  ## Examples

      iex> new_workout()
      %Workout{muscles: []}

  """
  def new_workout() do
    %Workout{}
    |> Repo.preload(:muscles)
  end

  @doc """
  Creates a workout.

  ## Examples

      iex> create_workout(user, %{field: value})
      {:ok, %Workout{}}

      iex> create_workout(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_workout(attrs \\ %{}, %User{role: :trainer} = user) do
    %Workout{}
    |> Workout.changeset(attrs)
    |> Changeset.put_assoc(:user, user)
    |> Repo.insert()
  end

  @doc """
  Updates a workout.

  ## Examples

      iex> update_workout(workout, %{field: new_value})
      {:ok, %Workout{}}

      iex> update_workout(workout, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_workout(%Workout{} = workout, attrs) do
    workout
    |> Workout.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a workout.

  ## Examples

      iex> delete_workout(workout)
      {:ok, %Workout{}}

      iex> delete_workout(workout)
      {:error, %Ecto.Changeset{}}

  """
  def delete_workout(%Workout{} = workout) do
    Repo.delete(workout)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking workout changes.

  ## Examples

      iex> change_workout(workout)
      %Ecto.Changeset{data: %Workout{}}

  """
  def change_workout(%Workout{} = workout, attrs \\ %{}) do
    Workout.changeset(workout, attrs)
  end

  def search_changeset(search, attrs \\ %{}) do
    Workout.search_changeset(search, attrs)
  end

  def workout_is_favorite_query(query, user_id) do
    query
    |> select_merge([q], %{is_favorite: (fragment("SELECT COUNT(*) > 0 FROM favorite_workouts WHERE favorite_workouts.workout_id = ? AND favorite_workouts.user_id = ?", q.id, ^user_id))})
  end

  def workout_is_liked_query(query, user_id) do
    query
    |> select_merge([q], %{is_liked: (fragment("SELECT COUNT(*) > 0 FROM workout_likes WHERE workout_likes.workout_id = ? AND workout_likes.user_id = ?", q.id, ^user_id))})
  end

  def workout_likes_query(query) do
    query
    |> select_merge([q], %{like_count: (fragment("SELECT COUNT(0) FROM workout_likes WHERE workout_likes.workout_id = ?", q.id))})
  end

  def workout_views_query(query) do
    query
    |> select_merge([q], %{view_count: (fragment("SELECT COUNT(0) FROM workout_views WHERE workout_views.workout_id = ?", q.id))})
  end

  def user_workout_query(query, %User{role: :trainer, id: user_id}) do
    from q in query,
    where: q.user_id == ^user_id
  end
  def user_workout_query(query, %User{}), do: query

  alias LiveFit.Fitness.Category

  @doc """
  Returns the list of categories.

  ## Examples

      iex> list_categories()
      [%Category{}, ...]

  """
  def list_categories do
    Repo.all(Category)
  end

  @doc """
  Gets a single category.

  Raises `Ecto.NoResultsError` if the Category does not exist.

  ## Examples

      iex> get_category!(123)
      %Category{}

      iex> get_category!(456)
      ** (Ecto.NoResultsError)

  """
  def get_category!(id), do: Repo.get!(Category, id)

  @doc """
  Creates a category.

  ## Examples

      iex> create_category(%{field: value})
      {:ok, %Category{}}

      iex> create_category(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_category(attrs \\ %{}) do
    %Category{}
    |> Category.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a category.

  ## Examples

      iex> update_category(category, %{field: new_value})
      {:ok, %Category{}}

      iex> update_category(category, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_category(%Category{} = category, attrs) do
    category
    |> Category.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a category.

  ## Examples

      iex> delete_category(category)
      {:ok, %Category{}}

      iex> delete_category(category)
      {:error, %Ecto.Changeset{}}

  """
  def delete_category(%Category{} = category) do
    Repo.delete(category)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking category changes.

  ## Examples

      iex> change_category(category)
      %Ecto.Changeset{data: %Category{}}

  """
  def change_category(%Category{} = category, attrs \\ %{}) do
    Category.changeset(category, attrs)
  end

  alias LiveFit.Fitness.Muscle

  @doc """
  Returns the list of muscles.

  ## Examples

      iex> list_muscles()
      [%Muscle{}, ...]

  """
  def list_muscles do
    Repo.all(Muscle)
  end

  @doc """
  Returns the list of muscles matching the list of IDs.
  raises error for lists containing empty string: [""]

  ## Examples

      iex> get_muscles_by_id([1, 2])
      [%Muscle{}, ...]

  """
  def get_muscles_by_id(""), do: []
  def get_muscles_by_id(list) do
    from(m in Muscle, where: m.id in ^list)
    |> Repo.all
  end

  @doc """
  Gets a single muscle.

  Raises `Ecto.NoResultsError` if the Muscle does not exist.

  ## Examples

      iex> get_muscle!(123)
      %Muscle{}

      iex> get_muscle!(456)
      ** (Ecto.NoResultsError)

  """
  def get_muscle!(id), do: Repo.get!(Muscle, id)

  @doc """
  Creates a muscle.

  ## Examples

      iex> create_muscle(%{field: value})
      {:ok, %Muscle{}}

      iex> create_muscle(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_muscle(attrs \\ %{}) do
    %Muscle{}
    |> Muscle.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a muscle.

  ## Examples

      iex> update_muscle(muscle, %{field: new_value})
      {:ok, %Muscle{}}

      iex> update_muscle(muscle, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_muscle(%Muscle{} = muscle, attrs) do
    muscle
    |> Muscle.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a muscle.

  ## Examples

      iex> delete_muscle(muscle)
      {:ok, %Muscle{}}

      iex> delete_muscle(muscle)
      {:error, %Ecto.Changeset{}}

  """
  def delete_muscle(%Muscle{} = muscle) do
    Repo.delete(muscle)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking muscle changes.

  ## Examples

      iex> change_muscle(muscle)
      %Ecto.Changeset{data: %Muscle{}}

  """
  def change_muscle(%Muscle{} = muscle, attrs \\ %{}) do
    Muscle.changeset(muscle, attrs)
  end
end
