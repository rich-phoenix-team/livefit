defmodule LiveFit.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset
  import EctoEnum
  alias LiveFit.Accounts.User

  defenum RoleEnum, :role_enum, [:trainee, :trainer, :admin]

  @derive {Inspect, except: [:password]}
  schema "users" do
    field :email, :string
    field :password, :string, virtual: true
    field :hashed_password, :string
    field :role, RoleEnum
    field :confirmed_at, :naive_datetime
    field :is_followed, :boolean, virtual: true
    field :is_liked, :boolean, virtual: true

    many_to_many :followed,
      LiveFit.Accounts.User,
      join_through: LiveFit.Interactions.UserFollowing,
      join_keys: [follower_id: :id, followed_id: :id],
      on_replace: :delete

    many_to_many :favorite_workouts,
      LiveFit.Fitness.Workout,
      join_through: LiveFit.Interactions.FavoriteWorkout,
      on_replace: :delete

    many_to_many :workout_views,
      LiveFit.Fitness.Workout,
      join_through: LiveFit.Interactions.WorkoutView,
      on_replace: :delete

    many_to_many :workout_likes,
      LiveFit.Fitness.Workout,
      join_through: LiveFit.Interactions.WorkoutLike,
      on_replace: :delete

    field :workout_count, :integer, virtual: true
    has_many :workouts, LiveFit.Fitness.Workout
    timestamps()
  end

  def trainee?(%User{role: role}), do: role == :trainee
  def trainer?(%User{role: role}), do: role == :trainer
  def admin?(%User{role: role}), do: role == :admin

  def follow_changeset(trainee, trainer, follow) do
    trainee
    |> change
    |> put_assoc(:followed, get_followed_users(trainee, trainer, follow))
  end
  defp get_followed_users(%{followed: followed}, trainer, true), do: [trainer | followed]
  defp get_followed_users(%{followed: followed}, trainer, false), do: followed -- [trainer]


  @doc """
  A user changeset for registration.

  It is important to validate the length of both email and password.
  Otherwise databases may truncate the email without warnings, which
  could lead to unpredictable or insecure behaviour. Long passwords may
  also be very expensive to hash for certain algorithms.
  """
  def registration_changeset(user, attrs) do
    user
    |> cast(attrs, [:email, :password])
    |> validate_email()
    |> validate_password()
  end

  def user_registration_changeset(user, attrs) do
    user
    |> registration_changeset(attrs)
    |> cast(attrs, [:role])
    |> validate_role()
  end

  def trainee_registration_changeset(user, attrs) do
    user
    |> registration_changeset(attrs)
    |> put_change(:role, :trainee)
  end

  def trainer_registration_changeset(user, attrs) do
    user
    |> registration_changeset(attrs)
    |> put_change(:role, :trainer)
  end

  def admin_registration_changeset(user, attrs) do
    user
    |> registration_changeset(attrs)
    |> put_change(:role, :admin)
  end

  defp validate_role(changeset) do
    changeset
    |> validate_required([:role])
    |> validate_inclusion(:role, [:trainee, :trainer])
  end

  defp validate_email(changeset) do
    changeset
    |> validate_required([:email])
    |> validate_format(:email, ~r/^[^\s]+@[^\s]+$/, message: "must have the @ sign and no spaces")
    |> validate_length(:email, max: 160)
    |> unsafe_validate_unique(:email, LiveFit.Repo)
    |> unique_constraint(:email)
  end

  defp validate_password(changeset) do
    changeset
    |> validate_required([:password])
    |> validate_length(:password, min: 12, max: 80)
    |> validate_confirmation(:password, message: "does not match password")
    # |> validate_format(:password, ~r/[a-z]/, message: "at least one lower case character")
    # |> validate_format(:password, ~r/[A-Z]/, message: "at least one upper case character")
    # |> validate_format(:password, ~r/[!?@#$%^&*_0-9]/, message: "at least one digit or punctuation character")
    |> prepare_changes(&hash_password/1)
  end

  defp hash_password(changeset) do
    password = get_change(changeset, :password)

    changeset
    |> put_change(:hashed_password, Bcrypt.hash_pwd_salt(password))
    |> delete_change(:password)
  end

  @doc """
  A user changeset for changing the email.

  It requires the email to change otherwise an error is added.
  """
  def email_changeset(user, attrs) do
    user
    |> cast(attrs, [:email])
    |> validate_email()
    |> case do
      %{changes: %{email: _}} = changeset -> changeset
      %{} = changeset -> add_error(changeset, :email, "did not change")
    end
  end

  @doc """
  A user changeset for changing the password.
  """
  def password_changeset(user, attrs) do
    user
    |> cast(attrs, [:password])
    |> validate_password()
  end

  @doc """
  Confirms the account by setting `confirmed_at`.
  """
  def confirm_changeset(user) do
    now = NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second)
    change(user, confirmed_at: now)
  end

  @doc """
  Verifies the password.

  If there is no user or the user doesn't have a password, we call
  `Bcrypt.no_user_verify/0` to avoid timing attacks.
  """
  def valid_password?(%LiveFit.Accounts.User{hashed_password: hashed_password}, password)
      when is_binary(hashed_password) and byte_size(password) > 0 do
    Bcrypt.verify_pass(password, hashed_password)
  end

  def valid_password?(_, _) do
    Bcrypt.no_user_verify()
    false
  end

  @doc """
  Validates the current password otherwise adds an error to the changeset.
  """
  def validate_current_password(changeset, password) do
    if valid_password?(changeset.data, password) do
      changeset
    else
      add_error(changeset, :current_password, "is not valid")
    end
  end
end
